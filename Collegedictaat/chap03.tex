%Hack om overfull hbox te vermijden
\chapter[Verbeteringen aan digitale PID\hyp{}regelaars]{Verbeteringen aan digitale \iftoggle{ebook}{}{\\}PID\hyp{}regelaars}
\label{sec:verbeteringen}
\Cref{eq:pidt} beschrijft de algemene vergelijking voor een PID\hyp{}regelaar
in het tijdcontinue domein. In het tijddiscrete domein komt
dit overeen met het doorrekenen van de bijbehorende
differentievergelijking. Bij de praktische implementatie van
zo'n regelaar lopen we tegen problemen aan, die
bij een analoge regelaar niet altijd op zullen treden.

Een model van een typisch digitaal geregeld systeem is gegeven in \cref{fig:dcs}.

\begin{myFigure}
    \centering%
    \iftoggle{ebook}{\begin{adjustbox}{max width=\textwidth}}{}
    \begin{tikzpicture}[auto, thick, node distance=3.3cm, ->,>={Stealth[scale=1.2,inset=0.6pt]},shorten >=1pt]
        \tikzset{%
          block/.style = {draw, thick, rectangle, minimum height = 5em, minimum width = 3em, inner sep=1em, font=\sffamily\Large},
        } 
        \draw
            node [block] (sensor) {Sensor} 
            node [block, right of=sensor] (adc) {ADC}
            node [block, align=center, right of=adc] (dr) {Digitale\\regelaar}
            node [block, right of=dr] (dac) {DAC}
            node [block, right of=dac] (actuator) {Actuator}
            node [block, below of=dr, node distance=3cm] (proces) {Proces};
        \draw (sensor) -- (adc);
        \draw (adc) -- (dr);
        \draw (dr) -- (dac);
        \draw (dac) -- (actuator);
        \draw (actuator) |- (proces);
        \draw (proces) -| (sensor);
    \end{tikzpicture}%
    \iftoggle{ebook}{\end{adjustbox}}{}%
    \caption{Algemeen model van een PID\hyp{}geregeld systeem.}
    \label{fig:dcs}
\end{myFigure}

Een bepaalde eigenschap die we willen regelen van het systeem wordt
gemeten d.m.v. een sensor. Deze waarde wordt door een ADC gesampled.
Een actuator zorgt voor de nieuwe input van het systeem. De actuator
wordt aangestuurd vanuit een DAC, die de proceswaarde vanuit de
digitale regelaar ontvangt. Voor de regelaar gezien is dus dankzij de
DAC en de ADC de hele omgeving tijddiscreet geworden, en kunnen we het
in \cref{fig:pidt} getoonde model tekenen zoals in \cref{fig:pidk} gegeven is.

\begin{myFigure}
    \centering%
    \iftoggle{ebook}{\begin{adjustbox}{max width=\textwidth}}{}
    \begin{tikzpicture}[auto, thick, node distance=2.5cm, ->,>={Stealth[scale=1.2,inset=0.6pt]},shorten >=1pt]
        \draw
            node (text1) {$r[k]$} 
           	node [input, right of=text1, node distance=.5cm] (input1) {} 
           	node [sum, right of=input1, node distance=1.5cm] (sum1) {\suma}
           	node [block, right of=sum1] (pid) {{PID}}
            node [block, right of=pid, node distance=3.5cm] (proces) {Proces}
           	node [sum, right of=proces] (sum2) {\suma}
            node [input, above of=sum2] (input2) {}
            node [above of=input2, node distance=.4cm] {$n[k]$} 
            node [coordinate, right of=sum2, node distance=1cm] (ret1) {}
            node [coordinate, below of=sum2, node distance=1.5cm] (ret2) {}
            node [output, right of=ret1] (output1) {}
            node [right of=output1, node distance=.5cm] {$y[k]$}; 
        \draw (input1) -- node {$+$}(sum1);
        \draw (sum1) -- node[pos=.35] {$e[k]$} (pid);
        \draw (pid) -- node[pos=.35] {$u[k]$} (proces);
        \draw (proces) -- node {$+$} (sum2);
        \draw (input2) -- node {$+$} (sum2);
        \draw (sum2) -- (output1);
        \draw (ret1) |- (ret2) -| node[near end]{$-$} (sum1);
    \end{tikzpicture}
    \iftoggle{ebook}{\end{adjustbox}}{}
    \caption{Model van een tijddiscreet PID\hyp{}geregeld systeem}
    \label{fig:pidk}
\end{myFigure}

Bij een analoge regelaar zijn de meeste terugkoppellussen op een
elektrische manier begrenst in bandbreedte. Hierdoor is het niet
mogelijk om zeer grote en snelle veranderingen in bijv. het foutsignaal
te krijgen, waardoor de regeling instabiel gedrag kan vertonen.

Het berekenen van de differentievergelijking gebeurt volgens
\cref{eq:uk}. Grote verandering in bijvoorbeeld een variabele of een
parameter kunnen leiden tot grote wijzigingen in de output van de PID\hyp{}regelaar. 
Wat verder ook nog meespeelt, is dat de ingelezen waarde $y[k]$
vaak beïnvloed is door ruis $n[k]$.

Het negatieve effect van grote en snelle wijzigingen in het foutsignaal
kan onderdrukt worden door het errorsignaal $e[k]$ een specifieke
behandeling te geven, alvorens het door de PID\hyp{}regelaar gebruikt gaat
worden (\squote{preprocessing}). Een andere mogelijkheid is het aanpassen van de differentievergelijking
van de PID\hyp{}regelaar, zodat een deel van deze preprocessing direct in
het algoritme van de PID\hyp{}regelaar gebeurt. Deze mogelijkheden worden in dit hoofdstuk verder uitgewerkt. 

Aan het eind van dit hoofdstuk komen nog enkele praktische aspecten aan bod.

\section{Filteren van de differentiërende actie}
Vaak zijn er verstoringen in de gemeten waarden van de procesoutput
$y[k]$. Dit zijn vaak relatief hoge frequentiecomponenten. Indien een
D-actie gebruikt wordt in de PID\hyp{}regelaar, dan reageert deze op
veranderingen in het errorsignaal. Daarmee wordt deze actie dan ook
gevoelig voor deze hoge frequentiecomponenten. Dit kan weer leiden tot
een te grote uitsturing aan de uitgang van de PID\hyp{}regelaar. Dit kan
soms een ongewenst effect geven.

De D-actie kan begrensd worden, om een te grote uitsturing te voorkomen.
Meer gebruikelijk is het toevoegen van een eenvoudig
1\textsuperscript{e}{}- of 2\textsuperscript{e}{}-orde
laagdoorlaatfilter, zodat de versterking lager wordt voor hogere
frequenties.

We gaan uit van de in \cref{eq:hrs} gegeven Laplace\hyp{}getransformeerde van de overdracht van een PID\hyp{}regelaar:
\begin{equation}
\tag{\ref{eq:hrs}}
\eqhrs
\end{equation}

We kennen de overdracht in het Laplace\hyp{}domein van een laagdoorlaatfilter
(\textbf{l}ow\-\textbf{p}ass):
\begin{equation}
H_{lp}(s)=\frac{1}{1+\gamma\cdot{}s}
\end{equation}

Als we dit toevoegen aan de D-actie van \cref{eq:hrs} resulteert
dit in:
\begin{equation}
\label{eq:hrls}
H_{rl}(s)=\frac{U(s)}{E(s)}=K_c\left(1+\frac{1}{T_i{\cdot}s}+T_d{\cdot}\frac{s}{1+\gamma\cdot{}s}\right)
\end{equation}

Hierbij is  $\gamma $ een kleine tijdconstante die in de praktijk meestal ingesteld wordt op ongeveer 10\% van de  $T_d$-waarde (bij de meeste industriële PID\hyp{}regelaars  
varieert $\gamma$ tussen 6,25\% en 12,5\% van de waarde van  $T_d$ \cite{Ang2005}).

Als we \cref{eq:hrls} willen implementeren in een digitale regelaar dan moet deze vergelijking getransformeerd worden naar het $z$\hyp{}domein. Als voorbeeld hebben we hier gebruik gemaakt van de directe transformatie via de Backward-Euler-methode, zie \cref{eq:bwe}.

Uiteindelijk komen we bij deze afleiding uit op de volgende overdracht
(een volledige afleiding voor Backward Euler wordt in het college
besproken):
\newcommand{\eqhrz}[1]{
H_r(z)=#1K_c\left(\frac{\gamma
+T_s+\displaystyle\frac{\gamma
T_s}{T_i}+\displaystyle\frac{T_s^{2}}{T_i}+T_d-{z}^{-1}\left(2\gamma
+T_s+\displaystyle\frac{\gamma
T_s}{T_i}+2T_d\right)+{z}^{-2}\left(T_d+\gamma
\right)}{\gamma +T_s-{z}^{-1}\left(T_s+2\gamma \right)+\gamma
{z}^{-2}}\right)
}
\iftoggle{ebook}{
\begin{multline}
\eqhrz{\\}
\end{multline}
}{
\begin{equation}
\eqhrz{}
\end{equation}
}

Door dezelfde stappen toe te passen als in de voorgaande voorbeelden
kunnen we deze overdracht terugtransformeren naar het tijddiscrete
domein. Let er op dat in de bovenstaande vergelijking onder de deelstreep
nu factoren bij de verschillende macht-termen van $z$ staan. Als we dit
omwerken naar de algemene vorm krijgen we ook coëfficiënten in het
recursieve deel. Deze noemen we $p$.

De uitgang van de regelaar wordt nu gegeven door:
\begin{equation}
\label{eq:hrlk}
u[k]=p_{1}\cdot{}u[k-1]+p_{2}\cdot{}u[k-2]+q_{0}\cdot{}e[k]+q_{1}\cdot{}e[k-1]+q_{2}\cdot{}e[k-2]
\end{equation}
Met de volgende coëfficiënten:
\begin{align*}
p_{1}&=\frac{T_s+2\gamma{}}{T_s+\gamma{}} &
p_{2}&=\frac{-\gamma{}}{T_s+\gamma{}} \displaybreak[0]\\
q_{0}&=K_c\left(1+\frac{T_s}{T_i}+\frac{T_d}{T_s+2\gamma{}}\right) &
q_{1}&=\frac{-{K_c}}{T_s+\gamma{}}\left(T_s+\gamma{}\left(2+\frac{T_s}{T_i}\right)+2T_d\right) \displaybreak[0]\\
q_{2}&=K_c\frac{T_d+\gamma{}}{T_s+\gamma{}}
\stepcounter{equation}\tag{\theequation}\label{eq:cbwe}
\end{align*}

Als we in plaats van de Backward-Euler-methode de bilineaire directe transformatiemethode toepassen (zie \cref{eq:blt}), dan krijgen we de
volgende coëfficiënten:
\begin{align*}
p_{1}&=\frac{4\gamma{}}{T_s+2\gamma{}} &
p_{2}&=\frac{T_s-2\gamma{}}{T_s+2\gamma{}} \displaybreak[0]\\
q_{0}&=K_c\left(1+\frac{T_s}{{2T}_{i}}+\frac{2T_d}{T_s+2\gamma{}}\right) &
q_{1}&=K_c\left(\frac{\displaystyle\frac{T_s^{2}}{T_i}-4\gamma{}-4T_d}{T_s+2\gamma{}}\right) \displaybreak[0]\\
q_{2}&=K_c\frac{2\gamma{}-T_s+\displaystyle\frac{T_s^{2}}{2T_i}-\displaystyle\frac{\gamma{}T_s}{T_i}+2T_d}{2\gamma{}+T_s}
\stepcounter{equation}\tag{\theequation}\label{eq:cblt}
\end{align*}

\begin{opdracht}{LPF in de D-actie}
	\label{opdr:lpfInD}
	Transformeer nu \cref{eq:hrls} zelf naar het tijddiscrete domein met behulp van de directe transformatie volgens
	\begin{enumerate}[label=\alph*)]
		\item de Backward-Euler-methode. Als het goed is levert dit \cref{eq:hrlk} op met de coëfficiënten uit \cref{eq:cbwe};
		\item de Backward-Euler-methode;
		\item de bilineaire methode. Als het goed is levert dit \cref{eq:hrlk} op met de coëfficiënten uit \cref{eq:cblt}.
	\end{enumerate}
\end{opdracht}

\section{PID\hyp{}regelaar type A, B en C}
In \cref{sec:directetransformatie} hebben we het snelheidsalgoritme van een PID\hyp{}regelaar met
behulp van Backward Euler afgeleid. De differentievergelijking werd
toen:
\iftoggle{ebook}{
\begin{multline}
\tag{\ref{eq:ukminukmin1}}
\equkminukmineen{\\}
\end{multline}
}{
\begin{equation}
\tag{\ref{eq:ukminukmin1}}
\equkminukmineen{}
\end{equation}
}

Hierbij is $e[k]$ het errorsignaal. Dit is gelijk aan het setpoint
(de referentiewaarde) minus de gemeten proceswaarde $y[k]$. Het
probleem bij deze implementatie van een PID\hyp{}regelaar is dat, als de
referentiewaarde op een andere waarde ingesteld wordt, dit direct
invloed heeft op de grootte van de proportionele actie, maar ook op de
grootte van de differentiërende actie. Hele snelle veranderingen in
het setpoint kunnen dus voor ongewenste effecten in de P- en D-actie zorgen! 
Een type regelaar waarbij dit probleem niet verholpen is
(zoals in \cref{eq:ukminukmin1}) \ wordt ook wel een
\squote{type A regelaar} genoemd.

Deze type A regelaar kan minder gevoelig gemaakt worden voor setpoint
veranderingen door het setpoint te verwijderen uit de
differentiërende actie. Dit kunnen we als volgt beredeneren; omdat we
alleen willen regelen met veranderingen op de lange termijn, kunnen we
aannemen dat r[k] (het setpoint) over een korte periode hetzelfde
blijft.

Dit betekent dat we kunnen aannemen dat: 
$r[k]\approx{}r[k-1]\approx{}r[k-2]\approx{}{\cdots}$.

Aangezien  $e[k]=r[k]-y[k]$ kunnen we
een substitutie maken in de D-actie. Nu gaat \cref{eq:ukminukmin1} over in:
\begin{multline}
u[k]-u[k-1] = K_c\left(e[k]-e[k-1] + \frac{T_s}{T_i}e[k]\right) + \displaybreak[0]\\ 
K_c\left(\frac{T_d}{T_s}\Big(r[k]-y[k]-2\big(r[k-1]-y[k-1]\big)+\big(r[k-2]-y[k-2]\big)\Big)\right)
\end{multline}

Observeer nogmaals dat $r[k]\approx{}r[k-1]\approx{}r[k-2]\approx{}{\cdots}$.
We kunnen nu een aantal $r[k]$'s in de D-actie tegen
elkaar wegstrepen. Dit resulteert in het wegvallen van het setpoint uit
de D-actie. Deze vorm wordt ook wel een \squote{Type B regelaar}
genoemd:
\newcommand{\eqtypeb}[1]{
u[k]-u[k-1]=K_c\bigg(e[k]-e[k-1]+\frac{T_s}{T_i}e[k]+#1\frac{T_d}{T_s}\big(-y[k]+2y[k-1]-y[k-2]\big)\bigg)
}
\iftoggle{ebook}{
\begin{multline}
\eqtypeb{\\}
\end{multline}
}{
\begin{equation}
\eqtypeb{}
\end{equation}
}

We kunnen ditzelfde principe ook toepassen op de proportionele actie. In
dat geval gaat de bovenstaande differentievergelijking over in:
\newcommand{\eqtypec}[1]{
u[k]-u[k-1]=K_c\bigg(-y[k]+y[k-1]+\frac{T_s}{T_i}e[k]+#1\frac{T_d}{T_s}\big(-y[k]+2y[k-1]-y[k-2]\big)\bigg)
}
\iftoggle{ebook}{
\begin{multline}
\label{eq:typec}
\eqtypec{\\}
\end{multline}
}{
\begin{equation}
\label{eq:typec}
\eqtypec{}
\end{equation}
}

Een regelaar waarbij het setpoint zowel uit de P- als de D-actie is
weggehaald wordt een \squote{Type C regelaar} genoemd. 

\section{Integrator wind-up}
% Integrator wind-up is een Engelse term daarom niet aan elkaar geschreven.
Een ander aandachtspunt bij het implementeren van digitale regelaars is
het verschijnsel van de zogenoemde integrator wind-up. Gegeven
\cref{eq:pidk}, die een discrete PID\hyp{}regelaar voorstelt, die met
behulp van FRM is bepaald:
\iftoggle{ebook}{
\begin{multline}
\tag{\ref{eq:pidk}}
\eqpidk{\\}
\end{multline}
}{
\begin{equation}
\tag{\ref{eq:pidk}}
\eqpidk{\;\;}
\end{equation}
}

Stel je de situatie voor dat het errorsignaal (om wat voor reden dan
ook\footnotemark{}) een flinke tijd positief is. Dit errorsignaal wordt
geïntegreerd in de tijd. In het geval van \cref{eq:pidk}
betekent dit dat de som bepaald wordt van alle voorgaande errorwaarden. 
De integrerende actie kan in dit geval behoorlijk groot worden.
\footnotetext{In de praktijk gebeurt dit vaak door zogenoemde actuatorverzadiging. Het proces is dan niet in staat om de output van de regelaar te volgen omdat de actuator al maximaal aangestuurd wordt.}

Het beste is om dit te illustreren met een voorbeeld: stel dat de errorwaarde $e[i]$ gelijk is aan 0,05 voor de laatste 50 waarden. De totale
somwaarde van de integrerende actie bedraagt in dit geval 50 * 0,05 =
2,5; oftewel 250 \% (uitgaande van het feit dat we daadwerkelijk over
de laatste 50 intervallen sommeren om de integraal te bepalen).

Stel nu dat het errorsignaal $e[i]$ vervolgens net iets kleiner wordt dan
0, bijvoorbeeld \hbox{$-0,02$}. Dan duurt het nog minstens 75 samples voordat
het integrerende deel van het outputsignaal weer terug bij 100\% is! 
Dit is natuurlijk een
%TODO: Er stond groot in plaats van langdurig. Klopt mijn verbetering? 
onacceptabel resultaat, dat kan leiden tot een langdurig overschot in het
te regelen proces.

Dit fenomeen staat bekend als integrator wind-up. De oplossing hiervoor
is om de totale waarde van de integrerende actie te beperken op de
maximale waarde van het outputsignaal. In het voorbeeld heeft het dus
zin om de integrerende actie te beperken tot 100\%.

\section{Beperkte precisie}
Binnen een digitaal systeem werken we niet alleen met discrete stappen
in de tijd, maar ook met discrete stappen in de waarden van het
setpoint, de procesvariabele, enz., omdat de waarden gerepresenteerd
worden door een eindig aantal bits.

In de praktijk wordt vaak gerekend met zogenoemde fixed-point getallen omdat
fixed-point berekeningen sneller zijn dan floating-point berekeningen en ook minder
energie kosten. Bij het gebruik van fixed-point getallen moeten we goed opletten dat we de  berekeningen met voldoende precisie uitvoeren.

Een voorbeeld: stel dat we gebruik maken van een 12-bit AD-converter.
Iedere stap in het $e[k]$\hyp{}signaal is nu $\frac{1}{{2}^{12}}=\frac{1}{4096}$ groot. 

Stel nu dat we de volgende vergelijking willen uitrekenen: 
${\Delta}{u}_{i}[k]=\frac{K_c\cdot{}T_s}{T_i}e[k]$, waarbij gegeven is 
dat $K_c\cdot{}T_s$ gelijk aan 1 is en $T_i$ gelijk aan 3550 is. 

Stel nu dat we dit naïef programmeren met behulp van 16-bits integer getallen als 
${\Delta}{u}_{i}[k]=\frac{e[k]}{3550}$.
Het errorsignaal zal dan minimaal 87\% van de
full-scale-waarde (4095 voor een 12-bit ADC) moeten bedragen om voor ${\Delta}{u}_{i}[k]$ een
waarde groter dan 0 berekend te krijgen.

Een minder naïeve implementatie zal gebruik maken van 16-bits fixed-point getallen met 12 bits voor en 4 bits na de decimale punt. 
We moeten dan $e[k]$ eerst vermenigvuldigen met 16 (4 plaatsen naar links schuiven). 
Er is geen gevaar voor overflow omdat het $e[k]$\hyp{}signaal afkomstig is van een 12-bit ADC. 
De berekening wordt dan uitgevoerd als ${\Delta}{u}_{i}[k]=\left(\frac{e[k]*16}{3550}\right)$. 
Het errorsignaal zal dan slechts minimaal 5,5\% van de
full-scale-waarde (4095 voor een 12-bit ADC) moeten bedragen om een
waarde groter dan 0 berekend te krijgen. Als dit nog steeds niet acceptabel is zal met 32-bits fixed-point getallen gerekend moeten worden waarbij 20 bits voor en 12 bits na de decimale punt gebruikt worden. In dit geval kunnen we de berekening uitvoeren als:
${\Delta}{u}_{i}[k]=\left(\frac{e[k]*4096}{3550}\right)$ en wordt de berekening met de hoogst mogelijke precisie uitgevoerd. 

Bij een incrementele regelaar (snelheidsalgoritme) wordt alleen het
verschil uitgerekend. Hierdoor kan de nauwkeurigheid wat groter worden
dan bij een absolute regelaar (positie algoritme).

Het werken met fixed-point berekeningen is soms onontkoombaar, bijv.
omdat de snelheid of het energiegebruik zeer belangrijk is. Maar met het krachtiger worden van
microcontrollers verdient het aanbeveling om te kijken of het mogelijk
is om toch met floating-point berekeningen te werken. Het vereenvoudigt
het implementeren van de regelaar in software aanzienlijk.

\section{Industriële regelaars}
De ontwikkeling van elektronica en in het bijzonder de
computertechnologie (microcontrollers) heeft er in geresulteerd dat vrijwel
alle commercieel verkrijgbare regelaars digitaal zijn en niet langer
analoge elementen bevatten, zoals dat jaren geleden nog wel
gebruikelijk was.

Praktische regelaars bestaan uit een of meerdere PID\hyp{}regelaars. PID\hyp{}regelaars zijn vaak beschikbaar
%TODO: functies vervangen door functieblokken. Is dit correct?
als functieblokken in PLC's en regelsystemen. De output van
zo'n regelaar is meestal analoog (analoge output, AO)
of digitaal (digitale output, DO). Meestal wordt de digitale output
gebruikt om een apparaat aan of uit te schakelen. Ook zijn er regelaars
met 2 digitale outputs: hierbij wordt de ene output gebruikt om een
apparaat aan of uit te schakelen en de andere output wordt gebruikt om
de richting aan te geven (omhoog/omlaag, links/rechts, verwarmen/koelen
etcetera).

Het analoge outputsignaal genereert meestal een spanningswaarde tussen
0 V en een maximale spanning. Het digitale outputsignaal is meestal
een in pulsbreedte gemoduleerd signaal van een bepaalde frequentie. De waarde van de regelaar
bepaalt de duty-cycle van het outputsignaal (bijv. als de output van de regelaar 
20\% is, dan is het signaal 20\% van de periodetijd hoog en 80\% van de
periodetijd laag).

Naast deze standaard outputs is er meestal ook een communicatiepoort
aanwezig. Dit is vaak een RS232/485 seriële interface.

Belangrijk bij iedere PID\hyp{}regelaar zijn de instellingen. Naast de
gebruikelijke waarden ($K_c$, $T_i$, $T_{d}$, $T_{s}$ en $\gamma$ voor het D-filter)
kunnen vaak ook de boven- en ondergrens van het outputsignaal van de regelaar
ingesteld worden.

Daarnaast kan de polariteit van de regelaar vaak ingesteld worden.
Hiermee wordt in feite het teken van de versterking ingesteld (+ of -).
Hiervoor worden de termen direct-acting (ook wel non-inverting of
heating-loop genoemd) en reverse-acting (ook wel inverting of
cooling-loop genoemd) gebruikt.

Bij een direct-acting controller is sprake van een positieve
versterking. Dit houdt in dat wanneer de output van de PID\hyp{}regelaar
verhoogd wordt, de procesvariabele ook groter zal worden. Bij een
verwarmingssysteem zal een grotere waarde van de output van de regelaar
meestal leiden tot een hogere temperatuur.

Bij een reverse-acting controller is sprake van een negatieve
versterking. Denk hier bijvoorbeeld aan een regelaar voor een koelkast.
Indien de output van de PID\hyp{}regelaar verhoogd wordt, zal de procesvariabele 
(de koelkast temperatuur) juist lager worden.

Andere veel voorkomende functies/instellingen van de huidige generatie
PID\hyp{}regelaars zijn:
\begin{itemize}
	\item Kunnen schakelen tussen handmatige bediening en automatisch regelen.
	\item De mogelijkheid hebben om de regelaar te programmeren, waarbij het
setpoint in de tijd geprogrammeerd kan worden (bijv. om een bepaalde cyclus
af te lopen).
	\item De mogelijkheid om meerdere regelaars achter elkaar te zetten
(cascade-loop).
	\item De mogelijkheid om de regelaar zelf de optimale PID\hyp{}parameters te laten bepalen
(auto-tuning). Meer hierover in het volgende hoofdstuk.
\end{itemize}
