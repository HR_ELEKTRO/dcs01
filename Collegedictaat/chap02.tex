\chapter{Digitale PID\hyp{}regelaars}
Een PID\hyp{}regelaar probeert de uitgangswaarde van een te regelen proces (in de Engelse literatuur kom je het woord \squote{plant} hiervoor regelmatig tegen) op een vooraf ingestelde waarde te houden. Deze waarde wordt ook wel de referentiewaarde of het setpoint (SP) genoemd.
De regelaar doet dit door de afwijking van de gewenste waarde te bepalen. Dit signaal noemt men vaak het \squote{error}\hyp{}signaal. Deze errorwaarde is gedefinieerd als het verschil tussen het setpoint en de uitgangswaarde (of \squote{output}) van het te regelen proces (bijv. een temperatuurwaarde of een waterhoogte). Deze uitgangswaarde wordt ook vaak de procesvariabele genoemd. In de praktijk bevat de uitgangswaarde ook wat ruis. In sommige systemen wordt hier rekening mee gehouden, maar in theoretische besprekingen van de basisprincipes van regelaars wordt dit vaak weggelaten. 
Hoe groter de errorwaarde, des te meer zal de regelaar bij moeten sturen. 
De meest gebruikte regelaar in de industrie is ontegenzeggelijk de PID\hyp{}regelaar. 
Hierbij staat de afkorting PID voor Proportioneel, Integrerend en Differentiërend. Met behulp van een drietal parameters (hierover later meer) kan de complete PID\hyp{}regelaar ingesteld worden.

We kunnen een typisch PID\hyp{}geregeld proces  weergeven met het wiskundige model van \cref{fig:pidt}.

\begin{myFigure}
    \centering%
    \iftoggle{ebook}{\begin{adjustbox}{max width=\textwidth}}{}
    \begin{tikzpicture}[auto, thick, node distance=2.5cm, ->,>={Stealth[scale=1.2,inset=0.6pt]},shorten >=1pt]
        \draw
            node (text1) {$r(t)$} 
           	node [input, right of=text1, node distance=.5cm] (input1) {} 
           	node [sum, right of=input1, node distance=1.5cm] (sum1) {\suma}
           	node [block, right of=sum1] (pid) {{PID}}
            node [block, right of=pid, node distance=3.5cm] (proces) {Proces}
           	node [sum, right of=proces] (sum2) {\suma}
            node [input, above of=sum2] (input2) {}
            node [above of=input2, node distance=.4cm] {$n(t)$} 
            node [coordinate, right of=sum2, node distance=1cm] (ret1) {}
            node [coordinate, below of=sum2, node distance=1.5cm] (ret2) {}
            node [output, right of=ret1] (output1) {}
            node [right of=output1, node distance=.5cm] {$y(t)$}; 
        \draw (input1) -- node {$+$}(sum1);
        \draw (sum1) -- node[pos=.35] {$e(t)$} (pid);
        \draw (pid) -- node[midway] {$u(t)$} (proces);
        \draw (proces) -- node {$+$} (sum2);
        \draw (input2) -- node {$+$} (sum2);
        \draw (sum2) -- (output1);
        \draw (ret1) |- (ret2) -| node[near end]{$-$} (sum1);
    \end{tikzpicture}
    \iftoggle{ebook}{\end{adjustbox}}{}
    \caption{Algemeen model van een PID\hyp{}geregeld systeem.}
    \label{fig:pidt}
\end{myFigure}

In dit model is:
\begin{description}[labelindent=2em, labelwidth=1cm, font=\normalfont, itemsep=-.5\baselineskip]
\item[$t$] tijdvariabele;
\item[$r(t)$] referentiewaarde oftewel setpoint;
\item[$e(t)$] afwijking of error t.o.v. de gewenste output;
\item[$u(t)$] output van de regelaar oftewel input van het proces;
\item[$n(t)$] een ruissignaal oftewel noise;
\item[$y(t)$] de uitgangswaarde, output oftewel procesvariabele.
\end{description}

Alle waarden die je in dit overzicht ziet zijn als functie van de tijd, $t$, gegeven. Bij DCS01 gaan we er uiteindelijk vanuit dat alle waarden kunnen veranderen in de tijd. Ook gaan we later bekijken wat er gebeurt als zelfs het proces zelf verandert in de tijd.

De algemene vergelijking voor een PID\hyp{}regelaar in het tijdcontinue domein wordt gegeven door:
\newcommand{\eqpidt}{%
u(t)=K_c\left(e(t) + \frac{1}{T_i}\int
_0^t{e(\tau) \,d\tau} + T_d\frac{de(t)}{dt}\right)%
}%
\begin{equation}
\label{eq:pidt}
\eqpidt
\end{equation}
Hierbij is:
\begin{description}[labelindent=2em, labelwidth=1cm, font=\normalfont, itemsep=-.5\baselineskip]
\item[$K_c$] Proportionele versterkingsfactor;
\item[$T_i$] Tijdconstante integrerende versterking [s];
\item[$T_d$] Tijdconstante differentiërende versterking [s].
\end{description}

Door middel van een Laplace\hyp{}transformatie kan deze tijdcontinue functie naar het $s$\hyp{}domein getransformeerd worden:
\begin{equation}\label{eq:pids}
U(s)=K_c\left(1 + \frac{1}{T_i \cdot{} s} + T_d \cdot{} s\right)E(s)
\end{equation}
Hierbij is $s$ de Laplace\hyp{}operator en is gelijk aan $\alpha{} + j\omega{}$. Uitgaande van \cref{eq:pids} kunnen we de overdrachtsfunctie bepalen van deze PID\hyp{}regelaar in het Laplace\hyp{}domein ($H_{r}(s)$):
\begin{equation}
\label{eq:hrs}
\eqhrs
\end{equation}

Digitale regelaars werken in het tijddiscrete domein in plaats van het tijdcontinue domein, omdat ze werken op samples van signalen en niet met het directe signaal zelf.

Om een tijddiscrete versie te krijgen van deze tijdcontinue PID\hyp{}regelaar, moeten we de integrerende en differentiërende componenten van \cref{eq:pidt} tijd-discreet maken. Hiervoor maken we eerst even een uitstapje naar twee deelparagrafen: eentje over numerieke integratie en eentje over numerieke differentiatie.

\section{Numerieke integratie}
Uit de integraalrekening weten we dat het bepalen van een integraal overeenkomt met het bepalen van het oppervlak onder de functie waarover we integreren. Het oppervlak onder de curve kunnen we benaderen door hier rechthoeken van te maken en het oppervlak van al deze rechthoeken bij elkaar op te tellen. In \cref{fig:frm} is dit concept weergegeven.

\newcommand{\figIntegraalBenadering}[3]{
\begin{myFigure}
    \centering%
    \begin{tikzpicture}[>={Stealth[scale=1.5,inset=0.6pt]},shorten >=1pt]
        \begin{axis}[
                width=0.9\textwidth, 
                height=0.45\textwidth,
                minor tick num=1, 
                axis lines*=middle, 
                xmin=0,
                xmax=15, 
                ymin=-2.5, 
                ymax=3.5,
                xlabel style={at={(ticklabel* cs:1.05)}, anchor=near ticklabel, align=center},
                xlabel={$\longrightarrow$ \\ $x$},
                ylabel style={at={(ticklabel* cs:.9)}, anchor=near ticklabel, align=center},
                ylabel={$e(x)$ \\ $\longrightarrow$ },
            ]
            \addplot[thick, smooth, hrred, mark=*] table {data/data1.dat};
            \addplot[#1, fill=gray, fill opacity=0.5] table {data/data1.dat};
            \addplot[ycomb, black, mark=none] table {data/data1.dat};
            \addplot[thick, dashed, blue] coordinates {(15,0)(15,1)}
            node[above, black] {t};
            \addplot[thick, dashed, blue] coordinates {(1,0)(1,-1.3)};
            \addplot[thick, dashed, blue] coordinates {(2,0)(2,-1.3)};
            \addplot[thick, blue] coordinates {(2,-1)};
            \node[coordinate] (a1) at (axis cs:1,-1) {};
            \node[coordinate] (a2) at (axis cs:2,-1) {};
            \draw[<->] (a1) -- (a2)
             node[anchor=north] at (axis cs:1.5,-1.3) {$T_s$};
        \end{axis}
    \end{tikzpicture}
    \caption{#2}
    \label{#3}
\end{myFigure}
}
\figIntegraalBenadering{const plot}{Forward Rectangular Method (FRM).}{fig:frm}

Als we nu iedere $T_s$ seconde een sample nemen van het
signaal, dan kan het oppervlak onder een functie $e(x)$ als
volgt benaderd worden:
\newcommand{\eqfrm}[1]{
\label{eq:frm}
\int_{0}^{t}{e(x)\,dx} \approx{} T_s\cdot{}e[0] + T_s\cdot{}e[1] + \cdots{} + T_s\cdot{}e[k-1] = T_s\sum_{i=1}^{k}{e[i-1]}#1\text{met}\; k = \left\lfloor{}\frac{t}{T_s} + \frac{1}{2}\right\rfloor{}
}
\iftoggle{ebook}{
\begin{multline}
\eqfrm{\\}
\end{multline}
}{
\begin{equation}
\eqfrm{\;\;}
\end{equation}
}

$k = \left\lfloor{}\frac{t}{T_s} + \frac{1}{2}\right\rfloor{}$ betekent dat bij deze 
formule $k$ de naar beneden afgeronde waarde van $\frac{t}{T_s} + \frac{1}{2}$ is.
Dat is hetzelfde als $\frac{t}{T_s}$ afronden naar een geheel getal.

De aanpak voor het benaderen van de integraal $e(x)$ van \cref{eq:frm} wordt ook wel de Forward Rectangular Method genoemd, of de
\squote{ondersom} nemen van de functie.

Een alternatieve manier om het oppervlak te bepalen is m.b.v. de
\squote{bovensom}. Dit wordt ook wel de
Backward Rectangular Method
(BRM) genoemd. Dit is weergegeven in \cref{fig:brm}.

\figIntegraalBenadering{const plot mark right}{Backward Rectangular Method (BRM).}{fig:brm}

Het oppervlak onder een functie $e(x)$ wordt bij deze methode als
volgt benaderd:
\begin{equation}
\label{eq:brm}%
\int_{0}^{t}{e(x)\,dx} \approx{} T_s\cdot{}e[1] + T_s\cdot{}e[2] + \cdots{} + T_s\cdot{}e[k] = T_s\sum_{i=1}^{k}{e[i]} \;\;\text{met}\; k = \left\lfloor{}\frac{t}{T_s} + \frac{1}{2}\right\rfloor{}
\end{equation}

Naast deze twee methoden is er nog een derde methode die vaak gebruikt
wordt. Dat is de \squote{trapeziummethode}
(TRAP). Hierin wordt het oppervlak onder een stapje van $T_s$ niet benaderd met een rechthoekje maar met een rechthoekig trapezium. De oppervlakte van zo'n trapezium is gelijk aan het gemiddelde van
2 opeenvolgende functiewaarden maal de sampletijd. Dit is meestal nauwkeuriger dan het gebruik van de FRM en BRM. Dit is goed te zien in \cref{fig:trap}.

\figIntegraalBenadering{sharp plot}{Trapeziummethode (TRAP).}{fig:trap}

Het oppervlak onder een functie $e(x)$ wordt bij deze methode als
volgt benaderd:
\begin{equation}
\begin{split}
\label{eq:trap}%
\int_{0}^{t}{e(x)\,dx} & \approx{} T_s\cdot{}\frac{e[1] + e[0]}{2} + T_s\cdot{}\frac{e[2] + e[1]}{2} + \cdots{} + T_s\cdot{}\frac{e[k] + e[k-1]}{2} \\ 
& = \frac{T_s}{2}\sum_{i=1}^{k}{e[i] + e[i-1]} \;\;\text{met}\; k = \left\lfloor{}\frac{t}{T_s} + \frac{1}{2}\right\rfloor{}
\end{split}
\end{equation}

\section{Numerieke Differentiatie}
Bij numerieke differentiatie vindt iets soortgelijks plaats. Uit de
differentiaalrekening weten we dat het differentiëren overeenkomt met
het bepalen van de raaklijn in een punt. Als benadering nemen we het verschil van twee opeenvolgende waarden van $e$ en delen dat door het verschil de bijbehorende waarden van $x$. In een formule:
\begin{equation}
\label{eq:dif2}%
e'(x) = \frac{de}{dx}
 \approx{} \frac{e[k] - e[k-1]}{T_s} \;\;\text{met}\; k = \left\lfloor{}\frac{t}{T_s} + \frac{1}{2}\right\rfloor{}
\end{equation}
Deze zogenoemde tweepuntsbenadering is zichtbaar in \cref{fig:dif2}.

\newcommand{\figDifferentiaalBenadering}[3]{
    \begin{myFigure}
        \centering%
        \begin{tikzpicture}[>={Stealth[scale=1.5,inset=0.6pt]},shorten >=1pt]
            \begin{axis}[
                    width=0.9\textwidth, 
                    height=0.45\textwidth,
                    minor tick num=1, 
                    axis lines*=middle, 
                    xmin=0,
                    xmax=15, 
                    ymin=-3, 
                    ymax=3.5,
                    xlabel style={at={(ticklabel* cs:1.05)}, anchor=near ticklabel, align=center},
                    xlabel={$\longrightarrow$ \\ $x$},
                    ylabel style={at={(ticklabel* cs:.9)}, anchor=near ticklabel, align=center},
                    ylabel={$e(x)$ \\ $\longrightarrow$ },
                ]
                \addplot[thick, smooth, hrred, mark=*] table {data/data1.dat};
                \addplot[ycomb, black, mark=none] table {data/data1.dat};
                \addplot[thick, dashed, blue] coordinates {(15,0)(15,1)} node[above, black] {t};
                \addplot[thick, dashed, blue] coordinates {(1,0)(1,-1.3)};
                \addplot[thick, dashed, blue] coordinates {(2,0)(2,-1.3)};
                \addplot[thick, blue] coordinates {(2,-1)};
                \node[coordinate] (a1) at (axis cs:1,-1) {};
                \node[coordinate] (a2) at (axis cs:2,-1) {};
                \draw[<->] (a1) -- (a2) node[anchor=north] at (axis cs:1.5,-1.3) {$T_s$};
                \addplot[thick, blue, quiver={u=1,v=\thisrow{v}},-{Stealth[scale=1,inset=0.6pt]}] table {#1};
            \end{axis}
        \end{tikzpicture}
        \caption{#2}
        \label{#3}
    \end{myFigure}
}
\figDifferentiaalBenadering{data/data2.dat}{Numeriek Differentiëren m.b.v. de tweepuntsbenadering.}{fig:dif2}

In plaats van de relatief eenvoudige tweepuntsbenadering voor het
differentiëren (zie \cref{eq:dif2}), kan ook gebruik gemaakt
worden van een driepunts- of een vierpuntsbenadering voor het
differentiëren. In dat geval gaat \cref{eq:dif2} over in:

Driepuntsbenadering:
\begin{equation}
\label{eq:dif3}%
e'(x) = \frac{de}{dx}
 \approx{} \frac{3\cdot{}e[k] - 4\cdot{}e[k-1] + e[k-2]}{2\cdot{}T_s} \;\;\text{met}\; k = \left\lfloor{}\frac{t}{T_s} + \frac{1}{2}\right\rfloor{}
\end{equation}
Deze driepuntsbenadering is zichtbaar in \cref{fig:dif3}.

\figDifferentiaalBenadering{data/data3.dat}{Numeriek Differentiëren m.b.v. de driepuntsbenadering.}{fig:dif3}

Vierpuntsbenadering:
\begin{equation}
\label{eq:dif4}%
e'(x) = \frac{de}{dx}
 \approx{} \frac{e[k+2] + 3\cdot{}e[k+1] - 3\cdot{}e[k] - e[k-1]}{6\cdot{}T_s} \;\;\text{met}\; k = \left\lfloor{}\frac{t}{T_s} + \frac{1}{2}\right\rfloor{}
\end{equation}
Bij deze vierpuntsbenadering wordt gebruik gemaakt van samples uit de toekomst. Deze vierpuntsbenadering is zichtbaar in \cref{fig:dif4}.

\figDifferentiaalBenadering{data/data4.dat}{Numeriek Differentiëren m.b.v. de vierpuntsbenadering.}{fig:dif4}

\iftoggle{ebook}{}{\needspace{3cm}}
Alternatieve vierpuntsbenadering:
\newcommand{\eqdifdrie}[1]{
\label{eq:dif4a}%
e'(x) = \frac{de}{dx}
 \approx{} \frac{11\cdot{}e[k] - 18\cdot{}e[k-1] + 9\cdot{}e[k-2] - 2\cdot{}e[k-3]}{6\cdot{}T_s}#1\text{met}\; k = \left\lfloor{}\frac{t}{T_s} + \frac{1}{2}\right\rfloor{}
}
\iftoggle{ebook}{
\begin{multline}
\eqdifdrie{\\}
\end{multline}
}{
\begin{equation}
\eqdifdrie{\;\;}
\end{equation}
}
Deze vierpuntsbenadering maakt geen gebruik van samples uit de toekomst en is zichtbaar in \cref{fig:dif4a}.

\figDifferentiaalBenadering{data/data5.dat}{Numeriek Differentiëren m.b.v. de alternatieve vierpuntsbenadering.}{fig:dif4a}

\section{Afleiding van een tijddiscrete PID\hyp{}regelaar}
Met behulp van de bovenstaande vergelijkingen voor numeriek integreren
en differentiëren kunnen we de formule van een tijddiscrete PID\hyp{}regelaar afleiden. 

Als voorbeeld maken we gebruik van de Forward Rectangular Method (FRM) (\cref{eq:frm}) om de integrator om te zetten en numerieke differentiatie met de tweepuntsbenadering (\cref{eq:dif2}) om de differentiator om te zetten.

Nogmaals gegeven de vergelijking van de uitgang van de regelaar \cref{eq:pidt}:
\begin{equation}
\tag{\ref{eq:pidt}}
\eqpidt
\end{equation}

Als we dit samplen met sampletijd $T_s$ en we passen de bovengenoemde numerieke methoden toe
dan krijgen we:
\iftoggle{ebook}{
\begin{multline}
\label{eq:pidk}
\eqpidk{\\}
\end{multline}
}{
\begin{equation}
\label{eq:pidk}
\eqpidk{\;\;}
\end{equation}
}
Waarbij $k$ de tijdvariabele van het tijddiscrete systeem is, oftewel het samplenummer.
De bovenstaande \cref{eq:pidk} wordt ook wel het \emph{positiealgoritme} genoemd en
kent in de praktijk een aantal nadelen.

\begin{opdracht}{Andere integratiemethodieken}
	\label{opdr:intmeth}
	Leid de uitgang van een tijddiscrete regelaar ook af met behulp van: 
	\begin{enumerate}[label=\alph*)]
		\item de BRM;
		\item de TRAP-methode.
	\end{enumerate}
\end{opdracht}

\section{Het snelheidsalgoritme}
Een groot nadeel van \cref{eq:pidk}, de tijddiscrete variant van de uitgang van de regelaar, is te vinden in het gedeelte van de integrator. In de praktijk willen we deze vergelijking graag gebruiken, maar dat betekent dat we alle voorgaande waarden van het errorsignaal $e[k]$ moeten opslaan. Dit komt omdat de formule nu zo geschreven is, dat het integrerende deel de som van alle voorgaande waarden van $e[k]$ bepaalt met behulp van een somreeks. De somreeks loopt over alle voorgaande waarden van $e[k]$.

We kunnen een praktijkvoorbeeld beschouwen. Stel je voor dat we een regeling hebben waarbij met een sample-rate van $T_s = 0,01\,\text{s}$, het toerental van een motor uitgelezen wordt met een nauwkeurigheid van 8 bits. Een ritje van Rotterdam naar Eindhoven duurt ongeveer 1.5 uur = 5400 seconden. Aan het eind van de rit hebben we dus 5400 / 0.01 = 540000 samples van een byte genomen die een geheugenruimte in beslag nemen van 540000 / 1024 $\approx{}$ 527 kilobyte. Dat lijkt niet veel, maar is voor een simpele en goedkope digitale regeling een hoop data! We hebben in de praktijk voor zulke hoeveelheden extra geheugenchips nodig om de data op te slaan. Dit maakt het systeem duurder.

Verder moet nu ook op het eind een somreeks bepaald worden over 540000 samples. Een ATmega328 van Atmel draaiend op 16 MHz (die zich bijv. ook op het \squote{Arduino} platform bevindt) zou er in theorie aan het eind van ons autoritje al minstens 0,034 seconden over doen om de integraal uit te rekenen. Dit is al langer dan de sample-rate van de regeling zelf; er is dus al lang geen tijd meer over om de samples in te lezen en de uitgang op tijd te bepalen. Om dit op te lossen zouden we een snellere en dus duurdere processor moeten kopen!

Een derde nadeel (buiten dit voorbeeld om) is dat een verandering in de proceswaarden $K_c$ of $T_i$ direct leidt tot een verandering in de waarde van de integrerende actie in zijn totaliteit. Dit kan bijvoorbeeld resulteren in overflows of andere dergelijke problemen. In de praktijk is dit vaak niet acceptabel omdat dit instabiliteit kan veroorzaken.

Om deze problemen tegen te gaan, kunnen we gebruik maken van een
recursieve variant van \cref{eq:pidk}, welke we het
\emph{snelheidsalgoritme} noemen. De afleiding van dit algoritme gaat
als volgt.

In plaats van elke keer de nieuwe uitgang van de regelaar, $u[k]$, te
bepalen, kunnen we ook de voorgaande waarde van $u[k]$ nemen, d.w.z.
$u[k-1]$, en het verschil met de gewenste $u[k]$, genaamd ${\Delta}u[k]$,
er bij optellen zodat:
\begin{equation}
\label{eq:deltauk}
u[k]=u[k-1]+{\Delta}u[k]
\end{equation}
We kunnen nu afleiden dat  ${\Delta}u\left[k\right]$ gegeven wordt
door:
\begin{equation}
\label{eq:deltaukdef}
{\Delta}u[k]=u[k]-u[k-1]
\end{equation}
We nemen nu de  $u[k]$ die afgeleid is met de FRM in
\cref{eq:pidk}, nogmaals hieronder getoond:
\iftoggle{ebook}{
\begin{multline}
\tag{\ref{eq:pidk}}
\eqpidk{\\}
\end{multline}
}{
\begin{equation}
\tag{\ref{eq:pidk}}
\eqpidk{\;\;}
\end{equation}
}
Door in deze formule voor $k$ telkens $k-1$ in te vullen vinden we $u[k-1]$
\begin{equation}
u[k-1]={K}_{c}\left(e[k-1]+\frac{{T}_{s}}{{T}_{i}}\sum
	_{i=1}^{k-1}{e[i-1]}+\frac{{T}_{d}}{{T}_{s}}\big(e[k-1]-e[k-2]\big)\right)
\end{equation}
Nu kunnen we met behulp van \cref{eq:deltaukdef} ${\Delta}u[k]$ bepalen:
\begin{equation}
\label{eq:deltauk2}
{\Delta}u[k]={K}_{c}\left(e[k]-e[k-1]+\frac{{T}_{s}}{{T}_{i}}e[k-1]+\frac{{T}_{d}}{{T}_{s}}\big(e[k]-2e[k-1]+e[k-2]\big)\right)
\end{equation}
Nu dat ${\Delta}u[k]$ bekend is kunnen we dit weer invullen
in \cref{eq:deltauk}. De uitgang van de regelaar wordt nu gegeven
door:
\newcommand{\equkrec}[1]{
u[k]=u[k-1]+{K}_{c}\bigg(e[k]-e[k-1]+\frac{{T}_{s}}{{T}_{i}}e[k-1]+#1\frac{{T}_{d}}{{T}_{s}}\big(e[k]-2e[k-1]+e[k-2]\big)\bigg)
}
\iftoggle{ebook}{
\begin{multline}
\equkrec{\\}
\end{multline}
}{
\begin{equation}
\equkrec{}
\end{equation}
}
We zien hier een formule met een recursief gedeelte, namelijk 
$u[k-1]$. Dit heeft als voordeel dat we nu in de integrator
geen somreeks met alle voorgaande samples meer terugvinden. De waarde
van de integrator zit nu in $u[k-1]$ verwerkt. We hoeven dus in de
praktijk geen groot duur geheugen en geen snellere en duurdere
processors meer toe te passen!

Het snelheidsalgoritme kent ook een algemene, kortere notatie,
namelijk:
\begin{equation}
\label{eq:snelheidsalgoritme}
u[k]=u[k-1]+{q}_{0}e[k]+{q}_{1}e[k-1]+{q}_{2}e[k-2]
\end{equation}
met:
\begin{align}
	{q}_{0}&={K}_{c}\left(1+\frac{{T}_{d}}{{T}_{s}}\right) & {q}_{1}&={K}_{c}\left(\frac{{T}_{s}}{{T}_{i}}-1-\frac{2{T}_{d}}{{T}_{s}}\right) & {q}_{2}&=\frac{{K}_{c}{T}_{d}}{{T}_{s}} 
\end{align}
Aangezien alle $q$'s (voorlopig) constanten zijn, lijkt
\cref{eq:snelheidsalgoritme} veel op de (reeds bekende) algemene notatie voor
een IIR-filter. De PID\hyp{}regelaar in snelheidsalgoritme (oftewel de
recursieve vorm) is dan ook een specifieke implementatie van een
IIR-filter!

Er zijn nog een aantal andere aan de praktijk gerelateerde aspecten.
Voor al deze implementaties geldt dat de sampletijd $T_s$
klein moet zijn in verhouding tot de tijdconstante van het te regelen
proces.

Verder is een belangrijke eigenschap van PID\hyp{}regelaars dat ze
relatief ongevoelig zouden moeten zijn voor wijzigingen in de PID\hyp{}parameters. 
De hierboven beschreven regelaars zijn dat nog niet
voldoende. Op deze zaken komen we later in dit document nog terug.

\begin{opdracht}{Het snelheidsalgoritme.}
	\label{opdr:snelheidsalgoritme}
	\begin{enumerate}[label=\alph*)]
		\item Reken de afleiding van ${\Delta}u[k]$ na en controleer dat \cref{eq:deltauk2} klopt.
		\item Pas het snelheidsalgoritme ook toe op de formule voor $u[k]$ die je bij \cref{opdr:intmeth}a met behulp van de BRM hebt afgeleid. 
		\item Pas het snelheidsalgoritme ook toe op de formule voor $u[k]$ die je bij \cref{opdr:intmeth}b met behulp van de TRAP-methode hebt afgeleid.
		\item Waarom is het snelheidsalgoritme handiger om in software te implementeren dan het positiealgoritme?
	\end{enumerate}
\end{opdracht}

% commando voor tabel
% #1 OPTIONAL = placement argument for figure e.g. tb or H 
% #2 = caption tekst
% #3 = labelnaam=tab:#3
% #4 = tabu kolom definities
% #5 = kop tabel (eerste regel)
% #6 = inhoud tabel (rest van de regels)

\tabel{Antwoorden van \cref{opdr:snelheidsalgoritme}.}{antwopdrsnelheid}{cccc}{
	Coëfficiënten & FRM & BRM & TRAP
}{
%\cellcolor{hrred}\color{white} 
$q_0$ 
& $K_c\left(1 + \displaystyle\frac{T_d}{T_s}\right)$
& $K_c\left(1 + \displaystyle\frac{T_s}{T_i} + \displaystyle\frac{T_d}{T_s}\right)$ 
& $K_c\left(1 + \displaystyle\frac{T_s}{2T_i} + \displaystyle\frac{T_d}{T_s}\right)$ \\ \hline
%\cellcolor{hrred}\color{white} 
$q_1$
& $K_c\left(\displaystyle\frac{T_s}{T_i} - 1 - \displaystyle\frac{2T_d}{T_s}\right)$ 
& $-K_c\left(1 + \displaystyle\frac{2T_d}{T_s}\right)$ 
& $K_c\left(\displaystyle\frac{T_s}{2T_i} - 1 - \displaystyle\frac{2T_d}{T_s}\right)$ \\ \hline
%\cellcolor{hrred}\color{white} 
$q_2$ 
& $\displaystyle\frac{K_c T_d}{Ts}$ 
& $\displaystyle\frac{K_c T_d}{Ts}$ 
& $\displaystyle\frac{K_c T_d}{Ts}$
}

\section{Directe transformatiemethoden}
\label{sec:directetransformatie}
Een andere methode om van de tijdcontinue variant naar een tijddiscrete
variant van de PID\hyp{}regelaar over te gaan is door de Laplace\hyp{}getransformeerde van de PID\hyp{}regelaar direct om te zetten naar het
$z$\hyp{}domein. Hiervoor kunnen we drie methoden gebruiken.

De meest gebruikte methode is de Bilineaire Transformatie (BLT). In de
literatuur, maar ook in MATLAB, wordt deze transformatie ook wel
\squote{Tustin} transformatie genoemd. Twee
andere methoden zijn de \squote{Backward Euler}-
en \squote{Forward Euler}-methode.

De BLT heeft de volgende vorm:
\begin{equation}
\label{eq:blt}
s\approx\frac{2}{T_s}{\cdot}\frac{z-1}{z+1}=\frac{2}{T_s}{\cdot}\frac{1-{z}^{-1}}{1+{z}^{-1}}
\end{equation}
De Backward Euler gebruikt de volgende substitutie:
\begin{equation}
\label{eq:bwe}
s\approx\frac{z-1}{z{\cdot}T_s}
\end{equation}
De Forward Euler gaat als volgt:
\begin{equation}
s\approx\frac{z-1}{T_s}
\end{equation}

Als we bijvoorbeeld terugkijken naar \cref{eq:hrs}:
\begin{equation}
\tag{\ref{eq:hrs}}
\eqhrs
\end{equation}

Dan kunnen we nu $s$ substitueren met een van de bovenstaande vormen. Als voorbeeld gebruiken we
de Backward-Euler-methode. We kunnen nu de overdracht bepalen van de regelaar in het $z$\hyp{}domein door elke $s$ in \cref{eq:hrs} te vervangen door $\displaystyle\frac{z-1}{z{\cdot}T_s}$:
\begin{equation}
H_r(z)=K_c\left(1+\frac{1}{T_i{\cdot}\displaystyle\frac{z-1}{z{\cdot}T_s}}+T_d{\cdot}\frac{z-1}{z{\cdot}T_s}\right)
\end{equation}
Omdat we uiteindelijk de formule in de vorm $U(z)=\dots$ willen hebben, proberen we alles binnen een enkele breuk te krijgen, aangezien $H(z)=\displaystyle\frac{U(z)}{E(z)}$.

Dubbele breuk wegwerken en constanten naar voren halen:
\begin{equation}
H_r(z)=K_c\left(1+\frac{T_s}{T_i}\cdot\frac{z}{z-1}+\frac{T_d}{T_s}{\cdot}\frac{z-1}{z}\right)
\end{equation}
Omwerken naar gemeenschappelijke noemer:
{
\allowdisplaybreaks
\begin{align}
H_r(z)&=K_c\left(1+\frac{T_s}{T_i}\cdot\frac{1}{1-{z}^{-1}}+\frac{T_d}{T_s}{\cdot}\left(1-{z}^{-1}\right)\right) \\
&=K_c\left(1+\frac{\displaystyle\frac{T_s}{T_i}}{1-{z}^{-1}}+\frac{\displaystyle\frac{T_d}{T_s}{\cdot}\left(1-{z}^{-1}\right)}{1}\right) \\
&=K_c\left(\frac{1-{z}^{-1}}{1-{z}^{-1}}+\frac{\displaystyle\frac{T_s}{T_i}}{1-{z}^{-1}}+\frac{\displaystyle\frac{T_d}{T_s}{\cdot}\left(1-{z}^{-1}\right)^2}{{1-{z}^{-1}}}\right) \\
&=K_c\left(\frac{1-{z}^{-1}+\displaystyle\frac{T_s}{T_i}+\displaystyle\frac{T_d}{T_s}{\cdot}\left(1-2{z}^{-1}+z^{-2}\right)}{{1-{z}^{-1}}}\right)
\end{align}
}
Dus:
\begin{equation}
H_r(z)=\frac{U(z)}{E(z)}=K_c\left(\frac{1-{z}^{-1}+\displaystyle\frac{T_s}{T_i}+\displaystyle\frac{T_d}{T_s}{\cdot}\left(1-2{z}^{-1}+z^{-1}\right)}{{1-{z}^{-1}}}\right)
\end{equation}
Als we kruislings vermenigvuldigen vinden we:
\newcommand{\equzminuzmineen}[1]{
U(z)-U(z)\cdot{}z^{-1}=K_c\bigg(E(z)-E(z)\cdot{}{z}^{-1}+\displaystyle\frac{T_s}{T_i}\cdot{}E(z)+#1\displaystyle\frac{T_d}{T_s}{\cdot}\left(E(z)-2E(z)\cdot{}{z}^{-1}+E(z)\cdot{}z^{-2}\right)\bigg)
}
\iftoggle{ebook}{
\begin{multline}
\equzminuzmineen{\\}
\end{multline}
}{
\begin{equation}
\equzminuzmineen{}
\end{equation}
}
Terugtransformeren van het $z$\hyp{}domein naar het tijddomein geeft:
\iftoggle{ebook}{
\begin{multline}
\label{eq:ukminukmin1}
\equkminukmineen{\\}
\end{multline}
}{
\begin{equation}
\label{eq:ukminukmin1}
\equkminukmineen{}
\end{equation}
}
Deze vergelijking kan ook geschreven worden als:
\begin{equation}
\label{eq:uk}
u[k]=u[k-1]+{q}_{0}e[k]+{q}_{1}e[k-1]+{q}_{2}e[k-2]
\end{equation}
met:
\begin{align}
\label{eq:ukq}
{q}_{0}&=K_c\left(1+\frac{T_s}{T_i}+\frac{T_d}{T_s}\right) & {q}_{1}&={-K}_{c}\left(1+\frac{2T_d}{T_s}\right) & {q}_{2}&=\frac{K_{c}T_d}{T_s}
\end{align}

Wat valt je op aan \cref{eq:uk} en \cref{eq:ukq} in vergelijking met \cref{eq:snelheidsalgoritme} en de bij \cref{opdr:snelheidsalgoritme} gevonden waarden voor $q_0$, $q_1$ en $q_2$?
\begin{opdracht}{Forward Euler en BLT.}
\label{opdr:forwardEulerenBLT}
Probeer de bovenstaande afleiding ook te maken met:
\begin{enumerate}[label=\alph*)]
\item de Forward-Euler-methode; 
\item de Bilineaire-Transformatie-methode.
\end{enumerate}
Ga daarbij uit van \cref{eq:hrs}.
\end{opdracht}

\section{Samenvatting}
In dit hoofdstuk hebben we gezien hoe we van de tijdcontinue variant van
een PID\hyp{}regelaar kunnen overgaan naar een tijddiscrete variant van een
PID\hyp{}regelaar. Met behulp van numerieke methoden kunnen we het
positiealgoritme van de PID\hyp{}regelaar afleiden. Helaas kent deze vorm
een aantal implementatiebeperkingen. Met behulp van het
snelheidsalgoritme kunnen we een efficiëntere variant afleiden van de
PID\hyp{}regelaar. Verder kunnen we ook gebruik maken van directe
transformatiemethodieken, zoals de Forward Euler, Backward Euler en de
Bilineaire Transformatie. Deze methodieken komen van pas in het
volgende hoofdstuk, als we verbeteringen aan gaan brengen aan de
regelaars.
