\chapter{Het vinden van de PID\hyp{}parameters}
\label{sec:vindenPIDparameters}
Nu we de tijddiscrete regelaars hebben afgeleid en ook enkele
interessante eigenschappen en verbeteringen aan de regelaars hebben
besproken, rest de vraag: \dquote{Hoe stellen we de regelaar
eigenlijk in?}. In dit hoofdstuk wordt hier op
verschillende manieren (voor simpel te regelen systemen) antwoord op
gegeven.

Bij het vinden van de optimale set parameters voor een digitale PID\hyp{}regelaar 
denken we al snel aan moderne digitale signaalbewerking. Toch
is de basis hiervoor reeds begin jaren '40 gelegd, in
een tijd dat er nog geen digitale regelaars waren. De regelaars in die
tijd waren mechanische en pneumatische apparaten.

Het was in 1942 dat de heren
Ziegler en Nichols hun wereldberoemde artikel publiceerden:
\dquote{settings for automatic
controllers} \cite{Ziegler1942}. De inhoud van dit artikel wordt nog
steeds gebruikt. Sterker nog: in de komende paragrafen zullen we de
essentie van hun verhaal behandelen.

\section{Stap-responsie van een eerste-orde-proces met tijdvertraging}
Veel industriële processen, die geregeld dienen te worden, kunnen
gemodelleerd worden als een eerste-orde-systeem met tijdvertraging
(FOPTD = First Order Process with Time Delay). De overdrachtsfunctie
van zo'n eerste-orde-systeem wordt dan voorgesteld door:
\begin{equation}
\label{eq:foptd}
G(s)=\frac{Y(s)}{U(s)}=\frac{{K}_{sys}{e}^{-\theta{}s}}{\tau{}_{sys}s+1}
\end{equation}

Hierin is:
\begin{itemize}
\item 
$G(s)$: de overdrachtsfunctie van het te regelen proces.
\item
$Y(s)$: de output van het te regelen proces, dit wordt ook wel de
procesvariabele PV genoemd. De eenheid hangt af van het te regelen proces. Stel bijvoorbeeld dat er een temperatuur geregeld moet worden dan is de eenheid bijvoorbeeld \textdegree{}C. 
\item 
$U(s)$: de output van de PID\hyp{}regelaar en de input voor het te
regelen proces. Deze waarde wordt vaak uitgedrukt in een percentage van het bereik van het outputsignaal (0 tot 100\%).
\item
Dode tijd $\theta{}$: de tijdvertraging van het te regelen proces in seconden.
\item
${K}_{sys}$: de versterking van het te regelen proces.
De eenheid hangt af van het te regelen proces. Stel bijv. dat $Y(s)$
een temperatuur voorstelt, dan is de eenheid van $K_{sys}$
gelijk aan \textdegree{}C/\%.
\item 
$\tau{}_{sys}$: de tijdconstante van het te regelen
proces in seconden.
\end{itemize}

De stapresponsie van een FOPTD met $K_{sys}=1$ \textdegree{}C/\%, $\tau{}_{sys} = 2$ s en $\theta{} = 1$ s op een stap van 30 naar 70\% is gegeven in \cref{fig:openloop1}.

\begin{myFigure}
    \centering%
    \begin{tikzpicture}[>={Stealth[scale=1.5,inset=0.6pt]},shorten >=1pt]
        \begin{axis}[
                width=0.84\textwidth, 
                height=0.56\textwidth,
                minor tick num=1, 
                axis lines*=middle, 
                xmin=0,
                xmax=12, 
                ymin=20, 
                ymax=80,
                xlabel style={at={(ticklabel* cs:1.08,-8)}, anchor=near ticklabel, align=center},
                xlabel={$\longrightarrow$ \\ $t$ [s]},
                ylabel style={at={(ticklabel* cs:.92,15)}, anchor=near ticklabel, align=center},
                ylabel={$T$ [\textdegree{}C]} \\ $\longrightarrow$,
                grid=major,
            ]
            \addplot[thick, dashed, gray] coordinates {(1,20)(1,80)};
            \addplot[thick, dashed, gray] coordinates {(3,20)(3,80)};
            \addplot[thick, dashed, gray] coordinates {(0,30)(14,30)};
            \addplot[thick, dashed, gray] coordinates {(0,70)(14,70)};
            \addplot[thick, smooth, hrred, mark=none] table {data/openloop1.dat};
            \addplot[thick, blue] coordinates {(0.5,20)(3.5,80)};
            \addplot[thick, dashed, blue] coordinates {(1,20)(1,30)};
            \addplot[thick, dashed, blue] coordinates {(3,20)(3,70)};
            \node[coordinate] (t0) at (axis cs:0,22.5) {};
            \node[coordinate] (t1) at (axis cs:1,22.5) {};
            \node[coordinate] (t2) at (axis cs:3,22.5) {};
            \draw[<->] (t0) -- node[anchor=south] {$\theta{}$} (t1);
            \draw[<->] (t1) -- node[anchor=south] {$\tau{}_{sys}$} (t2);
        \end{axis}
    \end{tikzpicture}
    \caption{De stapresponsie van een FOPTD.}
    \label{fig:openloop1}
\end{myFigure}
De dode tijd $\theta{}$ is eenvoudig uit de stapresponsie af te lezen. Je ziet dat in \cref{fig:openloop1} geldt $\theta{} = 1$~s. Als we de raaklijn tekenen in het eindpunt van de dode tijd (de blauwe lijn in \cref{fig:openloop1}) dan kunnen we ook $\tau{}_{sys}$ aflezen. Je ziet dat in \cref{fig:openloop1} geldt $\tau{}_{sys} = 2$~s.  

Bedenk dat \cref{eq:foptd} een model is voor een proces dat we met een PID\hyp{}regelaar willen regelen. Geen enkel praktisch proces zal zich exact zo gedragen als het model. In de volgende paragraaf zullen we de methode bespreken die bedacht is door Ziegler en Nichols waarmee de instellingen voor de PID\hyp{}regelaar gevonden kunnen worden voor een proces waarvan de response \emph{lijkt} op de response van een FOPTD.

\section{Open-loop-methode van Ziegler-Nichols}
\label{sec:zieglernichols}
Stel dat we een proces dat zich \emph{ongeveer} gedraagt als een FOPTD willen regelen met een PID\hyp{}regelaar. We kunnen de instellingen van de PID\hyp{}regelaar volgens de open-loop-methode\footnote{De Engelse benaming is \squote{open-loop method}. Dit kan in het Nederlands vertaald worden als \squote{openkringmethode}. Omdat de Engelse termen \squote{open-loop} in de regeltechniek, ook in het Nederlands, vaak voorkomt wordt in dit dictaat de term open-loop-methode gebruikt.} van Ziegler en Nichols als volgt vinden\cite{Haugen2010}: 

\begin{enumerate}
\item Zorg ervoor dat de regellus (zie \cref{fig:pidt}) doorbroken wordt. Dit kan in de praktijk gedaan worden door de PID\hyp{}regelaar op handbediening te zetten.
\item Regel het proces handmatig zodanig dat de uitgang een waarde heeft die in de praktijk vaak als setpoint wordt gebruikt en wacht tot de uitgangswaarde constant blijft.
\item Breng nu handmatig een stap op de output van de regelaar (dat is de input van het proces) aan en registreer de stapresponsie. Het kiezen van een geschikte stapgrootte is een kwestie van ervaring. Het verdient in principe de voorkeur om de stapgrootte zo groot mogelijk te nemen, dat is namelijk het meest nauwkeurig. 
Aan de andere kant willen we het proces dat we regelen in sommige gevallen ook niet te veel verstoren 
alleen maar om de regelaar in te kunnen stellen.
We nemen als voorbeeld een temperatuurregeling (deze gedragen zich in de praktijk vrijwel altijd als een FOPTD) met een met $K_{sys}$ van $1$~\textdegree{}C/\% die we handmatig hebben ingesteld op 30\% wat dus overeenkomt met 30~\textdegree{}C. Vervolgens brengen we een stap aan van 30\% naar 70\%. De stapgroote in \% noemen we $\Delta{}p$. In dit voorbeeld geldt dus $\Delta{}p = 70\% - 30\% = 40\%$. De stapresponsie is gegeven in \cref{fig:openloop2}. Je ziet dat deze stapresponsie niet helemaal gelijk is aan de stapresponsie van een FOPTD (zie \cref{fig:openloop1}) maar dat het er voldoende op lijkt om de FOPTD als model te gebruiken.
\item Uit de stapresponsie moeten we nu 2 waarden aflezen:
\begin{itemize}
\item De genormaliseerde stijging van de stapresponsie. In dit document
wordt deze grootheid aangeduid met $a^*$. Om deze te kunnen vinden moeten we eerst de stijging van de stapresponsie, aangeduid met $a$ vinden. Deze kan gevonden worden door de helling te bepalen van de meest steile raaklijn aan de stapresponsie die we kunnen vinden\footnote{%
	We zoeken dus de helling van de raaklijn in het buigpunt van de stapresponsie.
}. 
In de tijd van Ziegler en Nichols was dit een kwestie van schatten, maar tegenwoordig kunnen we de stapresponse inlezen in een computer en de maximale waarde van de afgeleide opzoeken\footnote{%
	We kunnen het tijdstip waarop de afgeleide maximaal is eenvoudig vinden door de tweede afgeleide van de stapresponsie gelijk te stellen aan nul.
}. 
We zouden dit kunnen doen m.b.v. van MATLAB, maar moderne digitale regelaars kunnen dit ook zelf.
De waarde van $a^*$ is gelijk aan $a/\Delta{}p$.
\item De dode tijd $\theta$: hiervoor nemen we de tijd tussen de start van de stap en het tijdstip waarop de meest steile raaklijn aan de stapresponsie de beginwaarde van de output snijdt.
\end{itemize}
\begin{opdracht}{Bepalen van $a^*$ en $\theta$}
\label{opdr:bepalenastar}
Bepaal zelf de waarde van $a^*$ en $\theta$ uit de in \cref{fig:openloop2} gegeven stapresponsie.
\end{opdracht}
\item Op basis van deze waarden ($a^*$ en $\theta$) waren Ziegler en Nichols destijds al in staat om een PID-
of PI\hyp{}regelaar zodanig in te stellen, dat deze een goed
regelgedrag liet zien. De
PID\hyp{}regelparameters en PI\hyp{}regelparameters voor deze open-loop-methode van Ziegler-Nichols staan in \cref{tab:znopenloop} vermeld.
\needspace{6\baselineskip}
\begin{opdracht}{Bepalen van de parameters van de PID\hyp{}regelaar}
\label{opdr:znopenloop}
Bepaal met behulp van de bij \cref{opdr:bepalenastar} bepaalde waarden van $a^*$ en $\theta$ de parameters voor de PID\hyp{}regelaar van dit proces volgens de open-loop-methode van Ziegler-Nichols.
\end{opdracht}
\end{enumerate}

\begin{myFigure}
    \centering%
    \begin{tikzpicture}[>={Stealth[scale=1.5,inset=0.6pt]},shorten >=1pt]
        \begin{axis}[
                width=0.84\textwidth, 
                height=0.56\textwidth,
                minor tick num=1, 
                axis lines*=middle, 
                xmin=0,
                xmax=12, 
                ymin=20, 
                ymax=80,
                xlabel style={at={(ticklabel* cs:1.08,-8)}, anchor=near ticklabel, align=center},
                xlabel={$\longrightarrow$ \\ $t$ [s]},
                ylabel style={at={(ticklabel* cs:.92,15)}, anchor=near ticklabel, align=center},
                ylabel={$T$ [\textdegree{}C]} \\ $\longrightarrow$,
                grid=major,
            ]
            \addplot[thick, smooth, hrred, mark=none] table {data/openloop2.dat};
        \end{axis}
    \end{tikzpicture}
    \caption{De stapresponsie van het te regelen proces.}
    \label{fig:openloop2}
\end{myFigure}

% commando voor tabel
% #1 OPTIONAL = placement argument for figure e.g. tb or H 
% #2 = caption tekst
% #3 = labelnaam=tab:#3
% #4 = tabu kolom definities
% #5 = kop tabel (eerste regel)
% #6 = inhoud tabel (rest van de regels)

\tabel{PID\hyp{}parameters volgens de open-loop-methode van Ziegler-Nichols.}{znopenloop}{lccc}{
	Methode / Parameters & $K_c$ [\%/?] & $T_i$ [s] & $T_d$ [s]
}{
Ziegler-Nichols open-loop (PID) 
& $K_c=\displaystyle\frac{1,2}{\theta{}\cdot{}a^*}$
& $T_i=2,0\cdot{}\theta$ 
& $T_d=0,5\cdot{}\theta$  \\ \hline
%\cellcolor{hrred}\color{white} 
Ziegler-Nichols open-loop (PI) 
& $K_c=\displaystyle\frac{0,9}{\theta{}\cdot{}a^*}$ 
& $T_i=0,5\cdot{}\theta$ 
& $-$
}

Het voordeel van deze methode is de eenvoud. Door een stap op het
systeem te zetten, kan eigenlijk al heel snel een redelijke instelling
bepaald worden voor de PID\hyp{}regelaar. Van dit principe maken veel
auto-tuning functies in commercieel verkrijgbare regelaars dan ook
dankbaar gebruik. Merk op dat het bij deze methode \emph{niet} nodig is om de stapresponsie helemaal tot de eindwaarde te laten lopen. Zodra we het buigpunt gezien hebben kunnen we stoppen. 

De Ziegler-Nichols-methode is erg eenvoudig om te gebruiken maar levert in de praktijk meestal niet het gewenste gedrag op \cite[blz.~32-4]{Levine2011}.
De regelaar geeft in de meeste gevallen te weinig demping en de regelaar is te gevoelig voor variaties in de procesparameters. Om deze reden zijn er vele alternatieve methoden bedacht waarvan we er enkele zullen bespreken in \cref{sec:openloopalt}. 

\section{\texorpdfstring{Antwoorden van \cref{opdr:bepalenastar,opdr:znopenloop}}{Antwoorden van voorgaande opdrachten}}
\label{sec:antwTheta}
In \cref{fig:asterentheta} zie je hoe de waarden van $a$ en $\theta{}$ gevonden kunnen worden met behulp van de raaklijn door het buigpunt van de stapresponsie.

\begin{myFigure}
    \centering%
    \begin{tikzpicture}[>={Stealth[scale=1.5,inset=0.6pt]},shorten >=1pt]
        \begin{axis}[
                width=0.84\textwidth, 
                height=0.56\textwidth,
                minor tick num=1, 
                axis lines*=middle, 
                xmin=0,
                xmax=12, 
                ymin=20, 
                ymax=80,
                xlabel style={at={(ticklabel* cs:1.08,-8)}, anchor=near ticklabel, align=center},
                xlabel={$\longrightarrow$ \\ $t$ [s]},
                ylabel style={at={(ticklabel* cs:.92,15)}, anchor=near ticklabel, align=center},
                ylabel={$T$ [\textdegree{}C]} \\ $\longrightarrow$,
                grid=major,
            ]
            \addplot[thick, dashed, gray] coordinates {(1.28,20)(1.28,80)};
            \addplot[thick, dashed, gray] coordinates {(4.41,20)(4.41,80)};
            \addplot[thick, dashed, gray] coordinates {(0,30)(14,30)};
            \addplot[thick, dashed, gray] coordinates {(0,70)(14,70)};
            \addplot[thick, smooth, hrred, mark=none] table {data/openloop2.dat};
            \addplot[thick, blue] {12.765897*x + 13.662149};
            \addplot[thick, dashed, blue] coordinates {(1.28,20)(1.28,30)};
            \addplot[thick, dashed, blue] coordinates {(4.41,20)(4.41,70)};
            \addplot[thick, dashed, blue] coordinates {(4.41,30)(5.25,30)};
            \addplot[thick, dashed, blue] coordinates {(4.41,70)(5.25,70)};
            \node[coordinate] (t0) at (axis cs:0,22.5) {};
            \node[coordinate] (t1) at (axis cs:1.28,22.5) {};
            \node[coordinate] (t2) at (axis cs:4.41,22.5) {};
            \node[coordinate] (T1) at (axis cs:5,30) {};
            \node[coordinate] (T2) at (axis cs:5,70) {};
            \draw[<->] (t0) -- node[anchor=south] {$\theta{}$} (t1);
            \draw[<->] (t1) -- node[anchor=south] {$\Delta{}t$} (t2);
            \draw[<->] (T1) -- node[anchor=west] {$\Delta{}T$} (T2);
            \node[anchor=west] at (axis cs:5,75) {$a=\displaystyle\frac{\Delta{}T}{\Delta{}t}$};
        \end{axis}
    \end{tikzpicture}
    \caption{De stapresponsie van het te regelen proces met de raaklijn door het buigpunt.}
    \label{fig:asterentheta}
\end{myFigure}
 
Uit \cref{fig:asterentheta} lezen we af $\theta{}\approx{}1,3$ s, $\Delta{}t\approx{}4,4-1,3=3,1$ s en $\Delta{}T=70-30=40$ \textdegree{}C. We kunnen nu $a$ berekenen: {$a=\frac{\Delta{}T}{\Delta{}t}=40/3,1=12,9$  \textdegree{}C/s.

De waarde van $a^*$ kunnen we nu eenvoudig berekenen: $a^*=a/\Delta{}p=12,9/40=0,32$ \textdegree{}C/(s$\cdot{}$\%). Met behulp van \cref{tab:znopenloop} kunnen we nu de PID\hyp{}parameters eenvoudig berekenen:
\begin{align}
\label{eq:pid_kc}
K_c&=\frac{1,2}{\theta{}\cdot{}a^*}=\frac{1,2}{1,3\cdot{}0,32}=2,88\ \text{\%/\textdegree{}C} \\
T_i&=2,0\cdot{}\theta=2,0\cdot{}1,3=2,6\ \text{s} \\
T_d&=0,5\cdot{}\theta=0,5\cdot{}1,3=0,65\ \text{s} 
\end{align}}
\section{Andere open-loop-methoden}
\label{sec:openloopalt}
Bij de in \cref{sec:zieglernichols} besproken methode schatten we twee procesparameters waarmee we de PID\hyp{}parameters bepalen.
Betere instellingen voor de PID\hyp{}parameters kunnen gevonden worden door drie in plaats van twee procesparameters te bepalen. We schatten niet alleen de versterking en de dode tijd maar ook de tijdconstante van
het systeem. De eersten die dit bedachten waren \citeauthor{Cohen1953} \cite{Cohen1953} in \citeyear{Cohen1953}.

We bepalen weer de stapresponsie van de open-loop, net zoals in de vorige paragraaf. We laten de responsie nu echter wel lopen totdat de eindwaarde bereikt is. Dit kan overigens erg lang duren, zeker indien het
te regelen proces beschikt over een grote tijdconstante. Voor deze
meting is een nauwkeurig eindwaarde van groot belang (in \cref{fig:openloop2} is
dat 70 \textdegree{}C). Hoe nauwkeuriger deze gemeten wordt, des te
beter. Op basis hiervan wordt een $\Delta{}T$ bepaald ($\Delta{}T=T_{start}-T_{eind}$).

De tijdconstante van een systeem ($\tau{}_{sys}$) is 
bij een zuiver eerste-orde-systeem het tijdstip waarop de stapresponsie
gestegen is met 63,2\% van $\Delta{}T$\footnote{De stapresponsie van een zuiver eerste-orde-systeem in procenten van de verandering van het uitgangssignaal is gelijk aan $(1-e^{\frac{-t}{\tau{}_{sys}}})\cdot{}100\text{\%}$.
Als we $t=\tau{}_{sys}$ invullen geeft dit ongeveer 63,2\% en als we $t=\frac{1}{3}\tau{}_{sys}$ invullen geeft dit ongeveer 28,3\%.
}. Bij een systeem met dode tijd $\theta$ geldt dat het tijdstip waarop de stapresponsie
gestegen is met 63,2\% van $\Delta{}T$ gelijk is aan $\theta{}+\tau{}_{sys}$. Daarnaast wordt bij een zuiver eerste-orde-systeem een derde van de tijdconstante bereikt op het moment dat de stapresponsie gestegen is met 28,3\% van $\Delta{}T$. Bij een systeem met dode tijd $\theta$ geldt dat het tijdstip waarop de stapresponsie
gestegen is met 28,3\% van $\Delta{}T$ gelijk is aan $\theta{}+\frac{1}{3}\tau{}_{sys}$.

Als dus beide bovengenoemde tijdstippen (laten we ze respectievelijk $t_1$ en $t_2$ noemen) bepaald worden, dan hebben we een stelsel van twee vergelijken met twee onbekenden:
\begin{equation}
\begin{cases}
t_1=\theta{}+\frac{1}{3}\tau{}_{sys} \\
t_2=\theta{}+\tau{}_{sys} 
\end{cases}
\end{equation}
We kunnen de waarden van $\tau{}_{sys}$ en $\theta{}$ bepalen door dit stelsel op te lossen:
\begin{equation}
\label{eq:tautheta}
\begin{cases}
\tau{}_{sys}=\frac{3}{2}(t_2-t_1) \\
\theta{}=t_2-\tau{}_{sys} 
\end{cases}
\end{equation}

Om de versterking van het te regelen proces ($K_{sys}$) te
bepalen, wordt de output van het proces gedeeld door de inputwaarde (=
de output van de PID\hyp{}regelaar).

Met behulp van $K_{sys}$, $\tau{}_{sys}$ en $\theta$ kunnen we een aantal nieuwe methoden
bekijken. Want naast Ziegler en Nichols zijn vele anderen bezig geweest
met het vinden van optimale PID\hyp{}regelparameters. Een korte selectie van
de meest gebruikte methoden staat in \cref{tab:openloop}. De IEEE database bevat maar liefst 762 papers met zowel \squote{PID} als \squote{tuning} in de titel.

\newlength{\colw}
\settowidth{\colw}{open-loop (PID)}
\newcommand{\colone}[1]{
	\begin{minipage}[c]{\colw}
		#1 
	\end{minipage}
}
\tabelmaxpaginabreedgeschaald{PID\hyp{}parameters volgens verschillende open-loop-methoden.}{openloop}{lccc}{
	\colone{Methode /\\Parameters}	& $K_c$ [\%/?] & $T_i$ [s] & $T_d$ [s]
}{
\colone{Ziegler-Nichols\\open-loop (PID)}
& $K_c=\displaystyle\frac{1,2\cdot{}\tau{}_{sys}}{K_{sys}\cdot{}\theta{}}$
& $T_i=2,0\cdot{}\theta$ 
& $T_d=0,5\cdot{}\theta$  \\ \hline
%\cellcolor{hrred}\color{white} 
\colone{Ziegler-Nichols\\open-loop (PI)} 
& $K_c=\displaystyle\frac{0,9\cdot{}\tau{}_{sys}}{K_{sys}\cdot{}\theta{}}$ 
%TODO: vreemde afwijking met vorige tabel
& $T_i=3,33\cdot{}\theta$ 
& $-$ \\ \hline
\colone{Cohen-Coon\\(PID)} 
& $K_c=\displaystyle\frac{\tau{}_{sys}}{K_{sys}\cdot{}\theta{}}\cdot{}\left(\displaystyle\frac{\theta{}}{4\cdot{}\tau{}_{sys}}+\displaystyle\frac{4}{3}\right)$ 
& $T_i=\theta{}\cdot{}\displaystyle\frac{32\cdot{}\tau{}_{sys}+6\cdot{}\theta{}}{13\cdot{}\tau{}_{sys}+8\cdot{}\theta{}}$ 
& $T_d=\theta{}\cdot{}\displaystyle\frac{4\cdot{}\tau{}_{sys}}{2\cdot{}\theta{}+11\cdot{}\tau{}_{sys}}$ \\ \hline
\colone{Cohen-Coon\\(PI)} 
& $K_c=\displaystyle\frac{\tau{}_{sys}}{K_{sys}\cdot{}\theta{}}\cdot{}\left(\displaystyle\frac{\theta{}}{12\cdot{}\tau{}_{sys}}+\displaystyle\frac{9}{10}\right)$ 
& $T_i=\theta{}\cdot{}\displaystyle\frac{30\cdot{}\tau{}_{sys}+3\cdot{}\theta{}}{9\cdot{}\tau{}_{sys}+20\cdot{}\theta{}}$ 
& $-$ \\ \hline
\colone{minimum-ITAE\\(PID)} 
& $K_c=\displaystyle\frac{1,357}{K_{sys}}\cdot{}\left(\displaystyle\frac{\theta{}}{\tau{}_{sys}}\right)^{-0,947}$ 
& $T_i=\displaystyle\frac{\tau{}_{sys}}{0,842}\cdot{}\left(\displaystyle\frac{\theta{}}{\tau{}_{sys}}\right)^{0,738}$ 
& $T_d=0,381\cdot{}\tau{}_{sys}\cdot{}\left(\displaystyle\frac{\theta{}}{\tau{}_{sys}}\right)^{0,995}$ \\ \hline
\colone{minimum-ITAE\\(PI)} 
& $K_c=\displaystyle\frac{0,859}{K_{sys}}\cdot{}\left(\displaystyle\frac{\theta{}}{\tau{}_{sys}}\right)^{-0,977}$ 
& $T_i=\displaystyle\frac{\tau{}_{sys}}{0,674}\cdot{}\left(\displaystyle\frac{\theta{}}{\tau{}_{sys}}\right)^{0,680}$ 
& $-$ \\ \hline
\colone{AMIGO\\(PID)} 
& $K_c=\displaystyle\frac{1}{K_{sys}}\cdot{}\left(0,2+0,45\cdot{}\frac{\tau{}_{sys}}{\theta{}}\right)$
& $T_i=\displaystyle\frac{0,4\cdot{}\theta{}+0,8\cdot{}\tau{}_{sys}}{\theta{}+0,1\cdot{}\tau{}_{sys}}\cdot{}\theta{}$
& $T_d=\displaystyle\frac{0,5\cdot{}\theta{}\cdot{}\tau{}_{sys}}{0,3\cdot{}\theta{}+\tau{}_{sys}}$ \\ \hline
\colone{AMIGO\\(PID)} 
& $K_c=\displaystyle\frac{0,35\cdot{}\tau{}_{sys}}{K_{sys}\cdot{}\theta{}}$ 
& $T_i=13,4\cdot{}\theta$ 
& $-$ 
}

Hierbij wordt Cohen-Coon vaak voor trage processen (lange dode tijd) gebruikt. De mini\-mum\hyp{}ITAE \cite{Lopez1976}
zorgt ervoor dat de integraal van de absolute error vermenigvuldigd met de tijd geminimaliseerd wordt (ITAE staat voor: Integral  of  Time-weighted  Absolute
Error). Deze methode levert een nauwkeurige regelaar op. Beide methoden zijn echter, net zoals de Ziegler-Nichols-methode, gevoelig voor veranderingen in de procesparameters \cite{Levine2011}. De AMIGO (Approximate M-constraint Integral Gain Optimization) -methode \cite{Astrom2004} is de meest recente van de hier gegeven methoden en is ontwikkeld met het doel om robuste instellingen te vinden waarbij de PID\hyp{}regelaar minder gevoelig is voor variaties in de procesparameters.

Het zal duidelijk zijn dat de open-loop-methoden alleen gebruikt kunnen worden als het proces zelfregelend is. Als het systeem van zichzelf instabiel is moet een andere methode worden gebruikt om de PID\hyp{}parameters in te stellen, bijvoorbeeld een van de closed-loop-methoden die in \cref{sec:closedloop} worden behandeld.

\needspace{4cm}
\begin{opdracht}{Bepalen van de parameters van de PID\hyp{}regelaar volgens de minimum-ITAE-methode}
\label{opdr:bepalenITAE}
	\begin{enumerate}[label=\alph*)]
		\item Bepaal zelf de waarde van $K_{sys}$, $\tau{}_{sys}$ en $\theta$ uit de in \cref{fig:openloop2} gegeven stapresponsie met behulp van de in deze paragraaf beschreven methode.
		\item Bepaal met behulp van deze waarden de parameters voor de PID\hyp{}regelaar van dit proces volgens de minimum-ITAE-methode.
	\end{enumerate}
\end{opdracht}

\section{\texorpdfstring{Antwoorden van \cref{opdr:bepalenITAE}}{Antwoorden van de voorgaande opdracht}}
Uit \cref{fig:tau} lezen we af: $t_1\approx{}2,2$~s en $t_2\approx{}3,4$~s. Met behulp van \cref{eq:tautheta} bepalen we $\tau{}_{sys}$ en $\theta{}$:  $\tau{}_{sys}=\frac{3}{2}\cdot{}(3,4-2,2)=1,8$~s en $\theta{}=3,4-1,8=1,6 $~s. De waarde van $K_{sys}$ kan als volgt berekend worden $K_{sys}=\frac{\Delta{}T}{\Delta{}p}=\frac{40}{40}=1$~\textdegree{}C/\%.
 
\begin{myFigure}
    \centering%
    \begin{tikzpicture}[>={Stealth[scale=1.5,inset=0.6pt]},shorten >=1pt]
        \begin{axis}[
                width=0.84\textwidth, 
                height=0.56\textwidth,
                minor tick num=1, 
                axis lines*=middle, 
                xmin=0,
                xmax=12, 
                ymin=20, 
                ymax=80,
                xlabel style={at={(ticklabel* cs:1.08,-8)}, anchor=near ticklabel, align=center},
                xlabel={$\longrightarrow$ \\ $t$ [s]},
                ylabel style={at={(ticklabel* cs:.92,15)}, anchor=near ticklabel, align=center},
                ylabel={$T$ [\textdegree{}C]} \\ $\longrightarrow$,
                grid=major,
            ]
            \addplot[thick, dashed, gray] coordinates {(0,30)(14,30)};
            \addplot[thick, dashed, gray] coordinates {(0,70)(14,70)};
            \addplot[thick, dashed, blue] coordinates {(0,41.32)(4,41.32)};
            \addplot[thick, dashed, blue] coordinates {(0,55.28)(4,55.28)};
            \addplot[thick, dashed, blue] coordinates {(2.15,28)(2.15,50)};
            \addplot[thick, dashed, blue] coordinates {(3.43,28)(3.43,65)};
            \addplot[thick, smooth, hrred, mark=none] table {data/openloop2.dat};
            \node[coordinate] (t0a) at (axis cs:0,47) {};
            \node[coordinate] (t0b) at (axis cs:0,62) {};
            \node[anchor=north] (t1) at (axis cs:2.15,28) {$t_1$};
            \node[coordinate] (t1a) at (axis cs:2.15,47) {};
            \node[anchor=north] (t2) at (axis cs:3.43,28) {$t_2$};
            \node[coordinate] (t2b) at (axis cs:3.43,62) {};
            \node[coordinate] (T1) at (axis cs:11.5,30) {};
            \node[coordinate] (T2) at (axis cs:11.5,70) {};
            \draw[<->] (t0a) -- node[anchor=south] {$\theta{}+\frac{1}{3}\tau_{sys}$} (t1a);
            \draw[<->] (t0b) -- node[anchor=south] {$\theta{}+\tau_{sys}$} (t2b);
            \draw[<->] (T1) -- node[anchor=east] {$\Delta{}T$} (T2);
            \node[anchor=west] at (axis cs:4,41.32) {$T_{start}+0,283\cdot{}\Delta{}T$};
            \node[anchor=west] at (axis cs:4,55.28) {$T_{start}+0,632\cdot{}\Delta{}T$};
        \end{axis}
    \end{tikzpicture}
    \caption{De stapresponsie van het te regelen proces. Op tijdstip $t_1$ heeft de output de waarde $T_{start}+0,283\cdot{}\Delta{}T$ bereikt en op tijdstip $t_2$ heeft de output de waarde $T_{start}+0,632\cdot{}\Delta{}T$ bereikt.}
    \label{fig:tau}
\end{myFigure}

Met behulp van de bepaalde waarden voor $K_{sys}$, $\tau{}_{sys}$ en $\theta$ kunnen we met behulp van \cref{tab:openloop} de parameterwaarden van de PID\hyp{}regelaar vinden volgens de minimum-ITAE-methode:
\begin{align}
K_c&=\displaystyle\frac{1,357}{K_{sys}}\cdot{}\left(\displaystyle\frac{\theta{}}{\tau{}_{sys}}\right)^{-0,947} =
     \displaystyle\frac{1,357}{1}      \cdot{}\left(\displaystyle\frac{1,6}{1,8}\right)^{-0,947} = 1,52 \ \text{\%/\textdegree{}C} \\
T_i&=\displaystyle\frac{\tau{}_{sys}}{0,842}\cdot{}\left(\displaystyle\frac{\theta{}}{\tau{}_{sys}}\right)^{0,738} =
     \displaystyle\frac{1,8}        {0,842}\cdot{}\left(\displaystyle\frac{1,6}{1,8}\right)^{0,738} = 1,96 \ \text{s} \\
T_d&=0,381\cdot{}\tau{}_{sys}\cdot{}\left(\displaystyle\frac{\theta{}}{\tau{}_{sys}}\right)^{0,995} =
     0,381\cdot{}1,8\cdot{}\left(\displaystyle\frac{1.6}    {1,8}\right)^{0,995} = 0,61 \ \text{s}
\end{align}

\section{Closed loop response}
\label{sec:closedloop}
Een andere methode om de PID\hyp{}parameters te bepalen is de zogenoemde \squote{closed-loop}-responsie.
Bij deze methode is het de bedoeling om de 
\squote{lus te sluiten} en de regelaar te
laten regelen. De
$T_i$-waarde wordt zo groot mogelijk gemaakt en de
$T_d$-waarde zo klein mogelijk, waardoor ze effectief
uitgeschakeld zijn en er uitsluitend met de $K_c$-waarde
gewerkt wordt. De $K_c$-waarde van de regelaar wordt vervolgens
zodanig opgevoerd, dat het systeem begint te oscilleren. Deze methode wordt daarom ook wel de ``oscillatie methode'' genoemd. In hoofdstuk~\ref{sec:systeemidentificatie} wordt deze methode nog verder gecombineerd/uitgebreid met systeemidentificatie.

\begin{myFigure}
    \centering%
    \begin{tikzpicture}[>={Stealth[scale=1.5,inset=0.6pt]},shorten >=1pt]
        \begin{axis}[
                width=0.84\textwidth, 
                height=0.56\textwidth,
                minor tick num=1, 
                axis lines*=middle, 
                xmin=0,
                xmax=16, 
                ymin=0, 
                ymax=100,
                xlabel style={at={(ticklabel* cs:1.08,-8)}, anchor=near ticklabel, align=center},
                xlabel={$\longrightarrow$ \\ $t$ [s]},
                ylabel style={at={(ticklabel* cs:.9,15)}, anchor=near ticklabel, align=center},
                ylabel={$Y(t)$} \\ $\longrightarrow$,
                grid=major,
            ]
            \addplot[thick, dashed, blue] coordinates {(6.14,0)(6.14,100)};
            \addplot[thick, dashed, blue] coordinates {(9.77,0)(9.77,100)};
            \addplot[thick, dashed, blue] coordinates {(12.28,10)(12.28,50)};
            \addplot[thick, dashed, blue] coordinates {(14.79,10)(14.79,50)};
            \addplot[thick, smooth, hrred, mark=none] table {data/closedloop.dat};
            \node[coordinate] (t0) at (axis cs:0,90) {};
            \node[coordinate] (t1) at (axis cs:6.14,90) {};
            \node[coordinate] (t2) at (axis cs:9.77,90) {};
            \node[coordinate] (t3) at (axis cs:16,90) {};
			\node[coordinate] (t4) at (axis cs:12.28,15) {};
			\node[coordinate] (t5) at (axis cs:14.79,15) {};
            \draw[<->] (t0) -- node[anchor=south] {$K_c=3$ \scriptsize (te klein)} (t1);
            \draw[<->] (t1) -- node[anchor=south] {$K_c=4$ \scriptsize (te groot)} (t2);
            \draw[<->] (t2) -- node[anchor=south] {$K_c=K_u=3,5$} node[anchor=north] {constante oscillatie}(t3);
            \draw[<->] (t4) -- node[anchor=north] {$T_u$} (t5);
        \end{axis}
    \end{tikzpicture}
    \caption{De responsie van het te regelen proces in closed loop bij verschillende waarden van $K_c$.}
    \label{fig:closedloop}
\end{myFigure}

De $K_c$-waarde waarbij het totale systeem begint te
oscilleren, wordt $K_u$ genoemd. De periodetijd van de
oscillaties, $T_u$, is ook van belang bij deze methode.
Uit \cref{fig:closedloop} kun je aflezen dat, in dit specifieke geval, de $K_c$-waarde waarbij het totale systeem begint te oscilleren, 3,5 is
en dat de periodetijd $T_u$ ongeveer 2,5 seconden is.

Een direct nadeel van deze methode is dat het in sommige installaties
ongewenst is om het systeem te laten oscilleren, het kan zelfs
gevaarlijk zijn. Wees je dus altijd bewust van het systeem waarmee je
aan het werk bent!

Ook hier zijn weer een aantal vuistregels te geven, hoe een regelaar
ingesteld kan worden. Een korte selectie van de meest gebruikte
methoden staat in \cref{tab:closedloop} \cite{Ziegler1942, Tyreus1992, Takahashi1997, Astrom2004}.

\settowidth{\colw}{closed loop (PID)}
\tabelmaxpaginabreedgeschaald{PID\hyp{}parameters volgens verschillende closed-loop-methoden.}{closedloop}{lccc}{
	\colone{Methode /\\Parameters}	& $K_c$ [\%/?] & $T_i$ [s] & $T_d$ [s]
}{
\colone{Ziegler-Nichols\\closed loop (PID)}
& $K_c=\displaystyle\frac{K_u}{1,7}$
& $T_i=\displaystyle\frac{T_u}{2}$ 
& $T_d=\displaystyle\frac{T_u}{8}$  \\ \hline
%\cellcolor{hrred}\color{white} 
\colone{Tyreus-Luyben} 
& $K_c=\displaystyle\frac{K_u}{2,2}$ 
& $T_i=2,2\cdot{}T_u$ 
& $T_d=\displaystyle\frac{T_u}{6,3}$ \\ \hline
\colone{Takahashi\\digitale regelaar} 
& $K_c=0,6\cdot{}K_u\cdot{}\left(1-\displaystyle\frac{T_s}{T_u}\right)$ 
& $T_i=\displaystyle\frac{T_u - T_s}{2}$ 
& $T_d=\displaystyle\frac{{T_u}^2}{8\cdot{T_u - T_s}}$ \\ \hline
\colone{AMIGO\\PID}
& $K_c=\left(0,3-0,1\cdot{}\kappa^4\right)\cdot{}K_u$ 
& $T_i=\displaystyle\frac{0,6}{1+2\cdot{}\kappa}\cdot{}T_u$ 
& $T_d=\displaystyle\frac{0,15\cdot{}(1-\kappa)}{1-0,95\cdot{}\kappa}\cdot{}T_u$ \\ \hline
\colone{AMIGO\\PI}
& $K_c=0,16\cdot{}K_u$ 
& $T_i=\displaystyle\frac{1}{1+4,5\cdot{}\kappa{}}\cdot{}T_u$ 
& $-$
}

Voor de closed-loop AMIGO-methoden geldt $\kappa{}=\left(\displaystyle\frac{1}{K_{sys}\cdot{}K_u}\right)$.

De methode van Takahashi is hier met name van belang. Het geeft een optimale
instelling voor de in \cref{sec:verbeteringen} afgeleide type C PID\hyp{}regelaar, zie \cref{eq:typec}. Een voordeel van deze methode is dat ook direct de sampletijd $T_s$ hierin meegenomen is.

\section{Industriële regelaars}
In \cite[blz.~32-19]{Levine2011} worden 17 industriële controllers opgesomd die automatisch de PID\hyp{}parameters kunnen bepalen. Tien daarvan gebruiken een open-loop-methode. Een andere methode die veel gebruikt wordt is de zogenoemde \squote{relay autotuner}. Bij deze methode wordt de PID\hyp{}regelaar vervangen door een relay met hysteresis. Voor veel processen zorgt dit ervoor dat het outputsignaal gaat oscilleren met periodetijd $T_u$. De waarde van $K_u$ kan door een amplitudemeting worden bepaald. Als de waarde van $T_u$ en $K_u$ bekend zijn, dan kunnen de PID\hyp{}parameters met behulp van de formules behorende bij één van de closed-loop-methoden (zie \cref{tab:closedloop}) bepaald worden. 
 
%TODO: Geschrapt. Methode wordt niet gebruikt in volgende hoofdstuk.
%Deze methode wordt
%dadelijk ook in \cref{sec:systeemidentificatie} gebruikt.

%\subsection{Praktijkvoorbeeld: bepalen van de dode tijd}
%TODO: Deze paragraaf geschrapt: voegt niets toe. Eens?

