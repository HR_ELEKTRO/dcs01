s = tf('s');
K = 40;
L = 1;
T = 2;
G = 30 + K * exp(-s*L) / (1+s*T);
[y,t] = step(G);
%figure;
title('');
xlabel('{\it t} [s]');
ylabel('{\it T} [\circ C]');
plot(t,y);
xlim([0 14]);
ylim([0 100]);
gray = [0.5 0.5 0.5];
line([1 1], [0 100], 'linestyle', ':', 'color', gray);
line([3 3], [0 100], 'linestyle', ':', 'color', gray);
line([0 14], [30 30], 'linestyle', ':', 'color', gray);
line([0 14], [70 70], 'linestyle', ':', 'color', gray);
line([0 4], [10 90], 'color', 'red');
text(1, -4, '\it t_1');
text(3, -4, '\it t_2');
text(-1.5, 30, '\it T_1');
text(-1.5, 70, '\it T_2');
%na een middagje frutten met MATLAB lukt het mij niet om nette plaatjes te
%maken. Daarom besloten om data weg te schrijven en te gebruiken in
%pgfplot
dlmwrite('openloop1.dat',downsample([t y],2),'delimiter','\t','precision',4);