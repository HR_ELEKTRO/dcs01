step=0.01;
eind1 = sqrt(12*pi);
eind2 = 2*eind1 - sqrt(2*pi);
eind3 = eind2+2*pi;
eind1 = round(eind1,3);
eind2 = round(eind2,3);
eind3 = round(eind3,3);
hold on
f1 = @(t) sin(0.5*t.^2).*(1-exp(-0.04*t.^2));
f2 = @(t) -f1(-t+2*eind1);
f3 = @(t) -0.3*sin(2.5*(t-eind2));
t = 0:step:eind3;
t1 = 0:step:eind1;
t2 = eind1+step:step:eind2;
t3 = eind2+step:step:eind3;
y = ([f1(t1) f2(t2) f3(t3)].*50)+40;
plot(t,y)
fprintf('eind1=%f, eind2=%f, eind3=%f\n', eind1, eind2, eind3);
dlmwrite('closedloop.dat',downsample([t' y'],10),'delimiter','\t','precision',4);