s = tf('s');
K = 40;
G = 30 + K * exp(-s*.5) / (1+3*s+2*s^2+s^3);
[y, t] = step(G);
figure1 = figure;
title('');
xlabel('{\it t} [s]');
ylabel('{\it T} [\circ C]');
hold on
plot(t,y);
xlim([0 14]);
ylim([0 100]);
gray = [0.5 0.5 0.5];
line([0 14], [30 30], 'linestyle', ':', 'color', gray);
line([0 14], [70 70], 'linestyle', ':', 'color', gray);
%deze lijnen zijn handmatig getekend en door MATLAB vertaald naar code BAH
%annotation(figure1, 'line', [0.375 0.377], [0.920 0.110], 'linestyle', ':', 'color', gray);
%annotation(figure1,'line',[0.2 0.2],[0.923 0.107], 'linestyle', ':', 'color', gray);
%annotation(figure1, 'line', [0.170 0.436], [0.295 0.79], 'color', 'red');
%text(1.1, -4, '\it t_1');
%text(4.3, -4, '\it t_2');
%text(-1.5, 30, '\it T_1');
%text(-1.5, 70, '\it T_2');
dy = [0;diff(y)/(t(2)-t(1))];
[dym,i]=max(dy);
fprintf('dy max = %f for t = %f and y = %f\n', dym, t(i), y(i));
%dus de raaklijn is:
a=dym;
b=y(i)-a*t(i);
l=a*t+b;
fprintf('raaklijn %f t + %f\n', a, b);
fprintf('dode tijd = %f\n', (30 - b)/a);
fprintf('t(raaklijk=70) = %f\n', (70 - b)/a);
TderdeTau=30+0.283*40;
[trash pos] = min(abs(y - TderdeTau));
tderdeTau=pos*(t(2)-t(1));
TTau=30+0.632*40;
[trash pos] = min(abs(y - TTau));
tTau=pos*(t(2)-t(1));
fprintf('t(derdetau) = %f bij y = %f\n', tderdeTau, TderdeTau);
fprintf('t(tau) = %f bij y = %f\n', tTau, TTau);

plot(t,l);
%na een middagje frutten met MATLAB lukt het mij niet om nette plaatjes te
%maken. Daarom besloten om data weg te schrijven en te gebruiken in
%pgfplot
dlmwrite('openloop2.dat',downsample([t y],2),'delimiter','\t','precision',4);
