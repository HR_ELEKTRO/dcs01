# Collegedictaat Digital Control Systems Hogeschool Rotterdam opleiding Elektrotechniek #

In dit repository is het collegedictaat opgenomen dat bij de module DCS01 (Digital Control Systems) van de opleiding Elektrotechniek aan de Hogeschool Rotterdam gebruikt wordt. Dit collegedictaat is geschreven in LaTeX.

**Deze module wordt niet meer aangeboden en is voor het laatst in het studiejaar 2018-2019 gegeven.**

Je kunt de gecompileerde versie (in pdf) vinden in de [Downloads](https://bitbucket.org/HR_ELEKTRO/dcs01/downloads) van dit repository.

Op- en aanmerkingen zijn altijd welkom. Maak een [issue](https://bitbucket.org/HR_ELEKTRO/dcs01/issues?status=new&status=open) aan of stuur een mail naar [Harry Broeders](mailto:j.z.m.broeders@hr.nl) en/of [Daniël Versluis](mailto:d.versluis@hr.nl).